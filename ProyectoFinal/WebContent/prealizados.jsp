<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"
	import="java.util.List, model.Pedidos,Servlets.login"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" href="style.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="./js/menu.js"></script>
<link rel="stylesheet" href="css/menu.css">

<title>Pedidos Realizados</title>
</head>
<body>
	<div class="wrapper">
		<header> <nav>
		<div class="menu-icon">
			<i class="fa fa-bars fa-2x"></i>
		</div>
		<div class="logo">LOGO</div>
		<div class="menu" name="menu">
			<ul>
				<li><a href="menu?accion=menu">Home</a></li>
				<%
					if (login.esAdmin) {
				%>
				<li><a href="menu?accion=usuario">Usuarios</a></li>
				<li><a href="menu?accion=producto">Productos</a></li>
				<%
					}
				%>
				<li><a href="menu?accion=ppendientes">Pedidos Pendientes</a></li>
				<li><a href="menu?accion=pprealizados">Pedidos Realizados</a></li>
			</ul>
		</div>
		<a></a> </nav> </header>
		<div class="content">
			<p>Pedidos Realizados</p>
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>ID</th>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Total</th>
						<th>Estado</th>
						<th>Accion</th>

						<th style="width: 30px;"></th>
					</tr>
				</thead>
				<%
					List<Pedidos> pedidos = (List<Pedidos>) request.getAttribute("Pedido");

					if (pedidos == null) {
				%>
				<h2>No hay Pedidos</h2>
				<%
					} else {
						for (int i = 0; i < pedidos.size(); i++) {
				%>
				<tbody>
					<tr>
						<td><%=pedidos.get(i).getPed_id()%></td>
						<td><%=pedidos.get(i).getPed_NombreUsuario()%></td>
						<td><%=pedidos.get(i).getPed_ApellidoUsuario()%></td>
						<td><%=pedidos.get(i).getTotal()%></td>
						<td><%=pedidos.get(i).getPed_State()%></td>
						<td><a
							href="PedidoServlet?id=<%=pedidos.get(i).getPed_id()%>&accion=VerDetalle"><i
								class="icon-pencil">Ver Detalle</i></a></td>
					</tr>
				</tbody>
				<%
					}
					}
				%>
			</table>

		</div>
	</div>
</body>
</html>
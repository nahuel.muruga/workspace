<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">

<script src="./js/login.js"></script>
<link rel="stylesheet" href="css/login.css">
<title>Ingreso</title>
</head>
<body>

	<form action="login" method="POST"> <!-- llama a login.java -->
		<section class="login-block">
		<div class="container">
			<div class="row">
				<div class="col-md-4 login-sec">
					<h2 class="text-center">Ingreso a la Aplicación</h2>
					<form class="login-form">
						<div class="form-group">
							<label for="exampleInputEmail1" class="text-uppercase">Username</label>
							<input type="text" name="user" class="form-control"
								placeholder="">
						</div>

						<div class="form-group">
							<label for="exampleInputPassword1" class="text-uppercase">Password</label>
							<input type="password" name="password" class="form-control"
								placeholder="">
						</div>

						<div class="form-check">
							<label class="form-check-label"> <input type="checkbox"
								class="form-check-input"> <small>Recuerdame</small>
							</label>
							<button type="submit" class="btn btn-login float-right"
								value="Ingresar">Ingresar</button>
							<button type="Reset" class="btn btn-login float-right"
								value="Limpiar">Limpiar</button>
							<!--     <p><input type="reset" value="Limpiar"></input></p> -->
						</div>

						<%if (request.getAttribute("error") != null) {%>
						<p style="color: red;"><%=request.getAttribute("error")%></p>
						<%}%>

					</form>

				</div>
				<div class="col-md-8 banner-sec">
					<div id="carouselExampleIndicators" class="carousel slide"
						data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#carouselExampleIndicators" data-slide-to="0"
								class="active"></li>
							<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
							<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
						</ol>
						<div class="carousel-inner" role="listbox">
							<div class="carousel-item active">
								<img class="d-block img-fluid"
									src="https://food.fnr.sndimg.com/content/dam/images/food/fullset/2018/10/22/0/FNK_INSTANT-POT-YOGURT-H-s4x3.jpg.rend.hgtvcom.826.620.suffix/1540214003587.jpeg"
									alt="First slide">
								<div class="carousel-caption d-none d-md-block">
									<div class="banner-text">
										<h2>Nuestros Productos</h2>
										<p>Esta Es la Imagen 1</p>
									</div>
								</div>
							</div>
							<div class="carousel-item">
								<img class="d-block img-fluid"
									src="https://food.fnr.sndimg.com/content/dam/images/food/fullset/2018/10/22/0/FNK_INSTANT-POT-YOGURT-H-s4x3.jpg.rend.hgtvcom.826.620.suffix/1540214003587.jpeg"
									alt="First slide">
								<div class="carousel-caption d-none d-md-block">
									<div class="banner-text">
										<h2>Nuestros Productos</h2>
										<p>Esta Es la imagen 2</p>
									</div>
								</div>
							</div>
							<div class="carousel-item">
								<img class="d-block img-fluid"
									src="https://images.clarin.com/2015/03/23/ryWk1qDCme_1256x620.jpg"
									alt="First slide">
								<div class="carousel-caption d-none d-md-block">
									<div class="banner-text">
										<h2>Nuestros Productos</h2>
										<p>Esta Es la imagen 3</p>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</section>
		<script
			src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
		<script
			src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	</form>
</body>
</html>

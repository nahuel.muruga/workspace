<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
    import ="java.util.List, model.Productos"
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	
<title>Nuevo Producto</title>
</head>
<body>
<form class="form-horizontal" action="ProductoServlet"  method="POST">
<fieldset>
 <% Productos u = (Productos)request.getAttribute("Producto");
 int id= u.getProd_Id();
 if(id == 0) {
	 %>
<h1 align="center"> Nuevo Producto <br /></h1>
<h3 align="center"> Todos los campos son requeridos <br /> </h3>
<br /> <br />
<!-- Text input-->
			
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="prod_Nombre">Nombre: </label>  
			  <div class="col-md-4">
			  <input id="prod_Nombre" name="prod_Nombre" type="text" placeholder="Nombre" class="form-control input-md" required=""maxlength="20" >
			  </div>
			</div>
			
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="prod_Contenido">Contenido: </label>  
			  <div class="col-md-4">
			  <input id="prod_Contenido" name="prod_Contenido" type="text" placeholder="Contenido" class="form-control input-md" required="" maxlength="15">
			  </div>
			</div>
			
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="prod_Precio">Precio: </label>  
			  <div class="col-md-4">
			  <input id="prod_Precio" name="prod_Precio" type="text" placeholder="Precio " class="form-control input-md" required=""maxlength="15" >
			  </div>
			</div>
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="prod_Presentacion">Presentacion: </label>  
			  <div class="col-md-4">
			  <input id="prod_Presentacion" name="prod_Presentacion" type="text" placeholder="Presentacion" class="form-control input-md" required="" maxlength="15">
			  </div>
			</div>
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="prod_proovedor">Proovedor: </label>  
			  <div class="col-md-4">
			  <input id="prod_Proovedor" name="prod_Proovedor" type="text" placeholder="Proovedor" class="form-control input-md" required="" maxlength="15">
			  </div>
			</div>
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="prod_Telefono">Tel�fono : </label>  
			  <div class="col-md-4">
			  <input id="prod_Telefono" name="prod_Telefono" type="text" placeholder="Tel�fono " class="form-control input-md" required="" maxlength="15">
			  </div>
			</div>
			
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="prod_tipoProd">Tipo de Producto: </label>  
			  <div class="col-md-4">
			  <input id="prod_TipoProd" name="prod_TipoProd" type="text" placeholder="Tipo de Producto" class="form-control input-md" required="" maxlength="15">
			  </div>
			</div>
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="prod_Stock">Stock: </label>  
			  <div class="col-md-4">
			  <input id="prod_Stock" name="prod_Stock" type="text" placeholder="Stock" class="form-control input-md" required="" maxlength="15">
			  </div>
			</div>
	<% } else { %>
	
	       <h1 align="center"> Editar Producto <br /></h1>
		   <h3 align="center"> Todos los campos son requeridos <br /> </h3>
           <br /> <br />


			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="textId">Id: </label>  
			  <div class="col-md-4">
			  <input id="textId" name="textId" type="text" placeholder="Id" class="form-control input-md" required="" value="<%=u.getProd_Id() %>" maxlength="15"> 
			  </div>
			</div>
			
			
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="prod_Nombre">Nombres: </label>  
			  <div class="col-md-4">
			  <input id="prod_Nombre" name="prod_Nombre" type="text" placeholder="Nombres" class="form-control input-md" required="" value="<%=u.getProd_Name() %>" maxlength="15"> 
			  </div>
			</div>
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="prod_Contenido">Contenido: </label>  
			  <div class="col-md-4">
			  <input id="prod_Contenido" name="prod_Contenido" type="text" placeholder="Contenido" class="form-control input-md" required="" value="<%=u.getProd_Contenido() %>" maxlength="15">
			  </div>
			</div>
			
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="prod_Precio">Precio: </label>  
			  <div class="col-md-4">
			  <input id="prod_Precio" name="prod_Precio" type="text" placeholder="Precio" class="form-control input-md" required=""  value="<%=u.getProd_Precio() %>" maxlength="15">
			  </div>
			</div>
			
		
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="prod_Presentacion">Presentacion: </label>  
			  <div class="col-md-4">
			  <input id="prod_Presentacion" name="prod_Presentacion" type="text" placeholder="Presentacion " class="form-control input-md" required="" value="<%=u.getProd_Presentacion() %>" maxlength="15">
			  </div>
			</div>
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="prod_Proovedor">Proveedor: </label>  
			  <div class="col-md-4">
			  <input id="prod_Proovedor" name="prod_Proovedor" type="text" placeholder="Proveedor" class="form-control input-md" required="" value="<%=u.getProd_Proovedor() %>" maxlength="15">
			  </div>
			</div>
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="prod_Telefono">Tel�fono : </label>  
			  <div class="col-md-4">
			  <input id="prod_Telefono" name="prod_Telefono" type="text" placeholder="Tel�fono " class="form-control input-md" required="" value="<%=u.getProd_Telefono() %>" maxlength="15">
			  </div>
			</div>
			
		
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="prod_Stock">Stock: </label>  
			  <div class="col-md-4">
			  <input id="prod_Stock" name="prod_Stock" type="text" placeholder="Stock" class="form-control input-md" required="" value="<%=u.getProd_Stock() %>" maxlength="15">
			  </div>
			</div>
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="prod_TipoProd">Tipo de Producto </label>  
			  <div class="col-md-4">
			  <input id="prod_TipoProd" name="prod_TipoProd" type="text" placeholder="Tipo de Producto" class="form-control input-md" required="" value="<%=u.getProd_TipoProd() %>" maxlength="15">
			  </div>
			</div>
			

	<%}%>
			
			<div class="btn-toolbar">
			    <div class="col-md-3">
					<button class="btn btn-primary" type="submit" >Aceptar</button 	>
					<a href="menu?accion=producto" class="btn btn-Info" type="submit" >Volver</a>
				</div>
			</div>



</fieldset>
</form>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
    import ="java.util.List, model.Usuario"
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Nuevo Usuario</title>
</head>

<body>

<form class="form-horizontal" action="UsuarioServlet"  method="POST">
<fieldset>
 <% Usuario u = (Usuario)request.getAttribute("Usuario");
 int id= u.getId();
 if(id == 0) {
	 %>
<h1 align="center"> Nuevo Usuario <br /></h1>
<h3 align="center"> Todos los campos son requeridos <br /> </h3>
<br /> <br />


			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="textNombres" >Nombres: </label>  
			  <div class="col-md-4">
			  <input id="textNombres" name="textNombres" type="text" placeholder="Nombres" class="form-control input-md" required="" maxlength="15">
			  </div>
			</div>
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="txtApellidos">Apellidos: </label>  
			  <div class="col-md-4">
			  <input id="txtApellidos" name="txtApellidos" type="text" placeholder="Apellidos" class="form-control input-md" required="" maxlength="15">
			  </div>
			</div>
			
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="txtDNI">DNI: </label>  
			  <div class="col-md-4">
			  <input id="txtDNI" name="txtDNI" type="text" placeholder="DNI" class="form-control input-md" required="" maxlength="8">
			  </div>
			</div>
			
			<!-- Select Basic -->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="selectEstadoCivil">Estado Civil: </label>
			  <div class="col-md-4">
			    <select id="selectEstadoCivil" name="selectEstadoCivil" class="form-control" >
			      <option value="1">Soltero</option>
			      <option value="2">Casado</option>
			      <option value="3">Divorciado</option>
			      <option value="4">Viudo</option> 
			    </select>
			  </div>
			</div>
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="ciudad">Ciudad: </label>  
			  <div class="col-md-4">
			  <input id="ciudad" name="ciudad" type="text" placeholder="Ciudad: " class="form-control input-md" required="" maxlength="15">
			  </div>
			</div> 
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="domicilio">Domicilio Negocio: </label>  
			  <div class="col-md-4">
			  <input id="domicilio" name="domicilio" type="text" placeholder="Domicilio Negocio" class="form-control input-md" required=""maxlength="10" >
			  </div>
			</div>
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="telefono">Tel�fono : </label>  
			  <div class="col-md-4">
			  <input id="telefono" name="telefono" type="text" placeholder="Tel�fono " class="form-control input-md" required="" maxlength="15" >
			  </div>
			</div>
			
						
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="email">E-mail: </label>  
			  <div class="col-md-4">
			  <input id="email" name="email" type="text" placeholder="E-mail" class="form-control input-md" required="" maxlength="15">
			  </div>
			</div>
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="usuario">Nombre de usuario: </label>  
			  <div class="col-md-4">
			  <input id="usuario" name="usuario" type="text" placeholder="Nombre de usuario" class="form-control input-md" required="" maxlength="15">
			  </div>
			</div>
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="pass">Password: </label>  
			  <div class="col-md-4">
			  <input id="pass" name="pass" type="text" placeholder="Password" class="form-control input-md" required="" maxlength="15">
			  </div>
			</div>
			
			<!-- Text input-->
			<div class="form-group" class="checkbox checkbox-primary">
			  <label class="col-md-4 control-label" for="admin">Admin: </label>  
			  <input class="col-md-4" id="admin" name="admin" type="checkbox" class="custom-control-input" >
			</div>

	<% } else { %>
	
	       <h1 align="center"> Editar Usuario <br /></h1>
		   <h3 align="center"> Todos los campos son requeridos <br /> </h3>
           <br /> <br />


			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="textId">Id: </label>  
			  <div class="col-md-4">
			  <input id="textId" name="textId" type="text" placeholder="Nombres" class="form-control input-md" required="" value="<%=u.getId()%>" maxlength="15"> 
			  </div>
			</div>
			
			
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="textNombres">Nombres: </label>  
			  <div class="col-md-4">
			  <input id="textNombres" name="textNombres" type="text" placeholder="Nombres" class="form-control input-md" required="" value="<%=u.getName()%>" maxlength="15"> 
			  </div>
			</div>
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="txtApellidos">Apellidos: </label>  
			  <div class="col-md-4">
			  <input id="txtApellidos" name="txtApellidos" type="text" placeholder="Apellidos" class="form-control input-md" required="" value="<%=u.getApellido()%>" maxlength="15">
			  </div>
			</div>
			
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="txtDNI">DNI: </label>  
			  <div class="col-md-4">
			  <input id="txtDNI" name="txtDNI" type="text" placeholder="DNI" class="form-control input-md" required=""  value="<%=u.getDni()%>" maxlength="8">
			  </div>
			</div>
			
			<!-- Select Basic -->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="selectEstadoCivil">Estado Civil: </label>
			  <div class="col-md-4">
			    <select id="selectEstadoCivil" name="selectEstadoCivil" class="form-control" value="<%=u.getEstadoCivil()%>" >
			      <option value="1">Soltero</option>
			      <option value="2">Casado</option>
			      <option value="3">Divorciado</option>
			      <option value="4">Viudo</option>
			    </select>
			  </div>
			</div>
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="ciudad">Ciudad: </label>  
			  <div class="col-md-4">
			  <input id="ciudad" name="ciudad" type="text" placeholder="Ciudad: " class="form-control input-md" required="" value="<%=u.getCiudad()%>"maxlength="15">
			  </div>
			</div>
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="domicilio">Domicilio </label>  
			  <div class="col-md-4">
			  <input id="domicilio" name="domicilio" type="text" placeholder="Domicio Negocio" class="form-control input-md" required="" value="<%=u.getDomicilio()%>" maxlength="10">
			  </div>
			</div>
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="telefono">Tel�fono : </label>  
			  <div class="col-md-4">
			  <input id="telefono" name="telefono" type="text" placeholder="Tel�fono " class="form-control input-md" required="" value="<%=u.getTelefono()%>" maxlength="15">
			  </div>
			</div>
			
		
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="email">E-mail: </label>  
			  <div class="col-md-4">
			  <input id="email" name="email" type="text" placeholder="E-mail" class="form-control input-md" required="" value="<%=u.getMail()%>" maxlength="15">
			  </div>
			</div>
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="usuario">Nombre de usuario: </label>  
			  <div class="col-md-4">
			  <input id="usuario" name="usuario" type="text" placeholder="Nombre de usuario" class="form-control input-md" required="" value="<%=u.getUser()%>" maxlength="15">
			  </div>
			</div>
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="pass">Password: </label>  
			  <div class="col-md-4">
			  <input id="pass" name="pass" type="text" placeholder="Password" class="form-control input-md" required="" value="<%=u.getPass()%>"maxlength="15" >
			  </div>
			</div>
			<!-- Text input-->
			<div class="form-group" class="checkbox checkbox-primary">
			  <label class="col-md-4 control-label" for="admin">Admin: </label>  
			  <input  class="col-md-4" id="admin2" name="admin" type="checkbox" class="custom-control-input" <% if (u.getAdmin()) {%> checked <%} %>>
			</div>
			
	<%}%>
			
			<div class="form-group" class="btn-toolbar">
			    <div class="col-md-3">
					<button class="btn btn-primary" type="submit" >Aceptar</button 	>
					<a href="menu?accion=usuario" class="btn btn-Info" type="submit" >Volver</a>
				</div>
			</div>



</fieldset>
</form>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"
	import="java.util.List, model.Usuario,Servlets.login"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" href="style.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">

<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="./js/menu.js"></script>
<script src="./js/usuario.js"></script>

<link rel="stylesheet" href="css/menu.css">



<title>Usuarios</title>
</head>
<body>
	<div class="wrapper">
		<header> <nav>
		<div class="menu-icon">
			<i class="fa fa-bars fa-2x"></i>
		</div>
		<div class="logo">LOGO</div>


		<div class="menu" name="menu">
			<ul>
				<li><a href="menu?accion=menu">Home</a></li>
				<% if( login.esAdmin){ %>
				<li><a href="menu?accion=usuario">Usuarios</a></li>
				<li><a href="menu?accion=producto">Productos</a></li>
				<% }%>
				<li><a href="menu?accion=ppendientes">Pedidos Pendientes</a></li>
				<li><a href="menu?accion=pprealizados">Pedidos Realizados</a></li>
			</ul>
		</div>
		<a></a> </nav> </header>
		<div class="content">
			<p>Administracion de usuarios</p>
		</div>
		<div class="btn-toolbar">
			<a class="btn btn-primary" type="submit"
				href="UsuarioServlet?accion=NuevoUsuario&id= null">New User</a>
		</div>
		<div class="well">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>ID</th>
						<th>UserName</th>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Email</th>
						<th>Telefono</th>
						<th>Admin</th>
						<th>Accion</th>

						<th style="width: 30px;"></th>
					</tr>
				</thead>
				<%
				      List<Usuario> usuarios = (List<Usuario>)request.getAttribute("usuarios");
				      
				      if (usuarios == null) {
				    	 %>
				<h2>No hay usuarios</h2>
				<% 
				      } else {
				      for(int i=0; i< usuarios.size();i++){
				    	      
				      %>
				<tbody>
					<tr>
						<td><%=usuarios.get(i).getId() %></td>
						<td><%=usuarios.get(i).getUser() %></td>
						<td><%=usuarios.get(i).getName() %></td>
						<td><%=usuarios.get(i).getApellido() %></td>
						<td><%=usuarios.get(i).getMail()%></td>
						<td><%=usuarios.get(i).getTelefono()%></td>
						<td><%=usuarios.get(i).getAdmin()%></td>
						<td><a
							href="UsuarioServlet?id=<%=usuarios.get(i).getId()%>&accion=EditarUsuario"type="link"> Editar </a> 
							<a
							href="UsuarioServlet?id=<%=usuarios.get(i).getId()%>&accion=EliminarUsuario"
							type="link">Eliminar</a></td>
					</tr>
				</tbody>
				<%
					}
					}
				%>
			</table>
		</div>

	</div>
</body>
</html>
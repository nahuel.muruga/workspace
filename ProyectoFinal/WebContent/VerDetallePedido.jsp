<%@page import="dao.ProductosDao"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"
	import="java.util.List, model.Pedidos,model.Productos,Servlets.login"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" href="style.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="./js/menu.js"></script>
<link rel="stylesheet" href="css/menu.css">

<title>Detalle de Pedido</title>
</head>
<body>
	<div class="wrapper">
		<header> <nav>
		<div class="menu-icon">
			<i class="fa fa-bars fa-2x"></i>
		</div>
		<div class="logo">LOGO</div>
		<div class="menu" name="menu">
			<ul>
				<li><a href="menu?accion=menu">Home</a></li>
				<%
					if (login.esAdmin) {
				%>
				<li><a href="menu?accion=usuario">Usuarios</a></li>
				<li><a href="menu?accion=producto">Productos</a></li>
				<%
					}
				%>
				<li><a href="menu?accion=ppendientes">Pedidos Pendientes</a></li>
				<li><a href="menu?accion=pprealizados">Pedidos Realizados</a></li>
			</ul>
		</div>
		<a></a> </nav> </header>
		<div class="content">
			<p>Detalle de Pedido</p>
			<%
				Pedidos u = (Pedidos) request.getAttribute("Pedido");
			    
			    
				int id = u.getPed_id();
			%>
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>ID</th>
						<th>Nombre Producto</th>
						<th>Presentacion</th>
						<th>Contenido</th>
						<th>Precio</th>
						<th>Cantidad</th>
						

						<th style="width: 30px;"></th>
					</tr>
				</thead>
				<%
					List<Productos> listProductos = u.getPedido_Producto();
					for (int i = 0; i < listProductos.size(); i++) {
						Productos producto = listProductos.get(i);
				%>
				<tbody>
					<tr>
						<td><%=producto.getProd_Id()%></td>
						<td><%=producto.getProd_Name()%></td>
						<td><%=producto.getProd_Presentacion()%></td>
						<td><%=producto.getProd_Contenido()%></td>
						<td><%=producto.getProd_Precio()%></td>
						<td><%=producto.getProd_CantProd()%></td>

						
					</tr>
				</tbody>

			</table>
			<%
				switch (u.getCodPed_State()) {
					case 1:
			%>
			<a
				href="PedidoServlet?id=<%=producto.getProd_Id()%>&accion=CambiarEstadoEnCurso"><i>Empezar
					Pedido</i></a> <a
				href="PedidoServlet?id=<%=producto.getProd_Id()%>&accion=CambiarEstadoCancelado"><i>Cancelar
					Pedido</i></a> <a
				href="PedidoServlet?id=<%=producto.getProd_Id()%>&accion=CambiarEstadoEnEspera"><i>No
					Hay stock de productos</i></a>

			<%
				break;
					case 2:
			%>
			<a
				href="PedidoServlet?id=<%=producto.getProd_Id()%>&accion=CambiarEstadoFinalizado"><i>Finalizar
					Pedido</i></a>
			</td> <a
				href="PedidoServlet?id=<%=producto.getProd_Id()%>&accion=CambiarEstadoCancelado"><i>Cancelar
					Pedido</i></a> <a
				href="PedidoServlet?id=<%=producto.getProd_Id()%>&accion=CambiarEstadoEnEspera"><i>No
					Hay stock de productos</i></a>
			<%
				break;

					}

				}
			%>
		</div>
	</div>
</body>
</html>
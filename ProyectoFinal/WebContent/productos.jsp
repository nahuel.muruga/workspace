<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
    import ="java.util.List, model.Productos,Servlets.login"
    
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
      
      <link rel="stylesheet" href="style.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <link  rel="stylesheet" href="css/menu.css">
      <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
      <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
      
	  <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
      <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
      <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
	  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	  <script src="./js/menu.js"></script>
	  <script src="./js/productos.js"></script>
      
	  
	  <title>Productos</title>
</head>
 <body>
 
      <div class="wrapper">
                 <header>
            <nav>
               <div class="menu-icon">
                  <i class="fa fa-bars fa-2x"></i>
               </div>
               <div class="logo">
                  LOGO
               </div>
               <div class="menu" name="menu" >
                  <ul>
                     <li><a href="menu?accion=menu">Home</a></li>
                      <% if( login.esAdmin){ %>
                     <li><a href="menu?accion=usuario" >Usuarios</a></li>
                     <li><a href="menu?accion=producto" >Productos</a></li>
                    <% }%> 
                     <li><a href="menu?accion=ppendientes">Pedidos Pendientes</a></li>
                     <li><a href="menu?accion=pprealizados">Pedidos Realizados</a></li>
                  </ul>
               </div>
             </nav>
         </header>
         <div class="content">
            <p>
            Esta es la pagina de Productos
            </p>
            
            <div class="container">
								<div class="row">
							        <div class="col-md-3">
							            
							                <div class="btn-toolbar">
										    <p><a class="btn btn-primary btn-lg" type="submit" href="ProductoServlet?accion=NuevoProducto&id= null">Nuevo producto</a></p>
										    </div>
							                
							                <div class="input-group">
							                    
							                    <input class="form-control" id="system-search" name="q" placeholder="Search for" required>
							                    
							                </div>
							            </form>
							        </div>
									<div class="col-md-12">
							    	<table class="table table-list-search">
							                    <thead>
							                        <tr>
							                            <th>Codigo</th>
							                            <th>Nombre Producto</th>
							                            <th>Presentacion</th>
							                            <th>Contenido</th>
							                            <th>Precio</th>
							                            <th>Stock</th>
							                            <th>Accion</th>
							                        </tr>
							                    </thead>
							                      <%    
														      List<Productos> producto = (List<Productos>)request.getAttribute("Producto");
							                                
														      if (producto == null) {
														    	  System.out.println(" aca llega vacio"); 
														    	 %> <h2>No hay Producto</h2> <% 
														      } else {
														    	 
														      for(int i=0; i<producto.size();i++){
														    	
										         %>
							                    <tbody>
													<tr>
											          <td><%=producto.get(i).getProd_Id() %></td>
											          <td><%=producto.get(i).getProd_Name() %></td>
											          <td><%=producto.get(i).getProd_Presentacion() %></td>
											          <td><%=producto.get(i).getProd_Contenido() %></td>
											          <td><%=producto.get(i).getProd_Precio() %></td>
											          <td><%=producto.get(i).getProd_Stock()%></td>
											          <td><a href="ProductoServlet?id=<%=producto.get(i).getProd_Id()%>&accion=EditarProducto" type="link" >Editar</a>
											          <a href="ProductoServlet?id=<%=producto.get(i).getProd_Id()%>&accion=EliminarProducto" type="link" >Eliminar</a></td>
											          
											        </tr>
							                    </tbody>
							                     <%  } } %>
							                </table>   
									</div>
								</div>
				</div>
         </div>
      </div>
   </body>
</html>
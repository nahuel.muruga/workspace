package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.sql.*;
import model.Pedidos_Estados;
import model.Pedidos_Productos;
import model.Pedidos;
import model.Productos;

public class PedidosDao {
	private Connection comm;
	public static final String Controlador = "com.mysql.jdbc.Driver"; // "org.apache.derby.jdbc.ClientDriver"; //
	public static final String URL = "jdbc:mysql://127.0.0.1:3306/proyectofinal_db"; // "jdbc:derby://localhost:1527/login";//
	public static final String USER = "root";
	public static final String PASS = "";

	static {
		try {
			Class.forName(Controlador);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public List<Pedidos> obtenerTodos() {
		List<Pedidos> Pedidos = new ArrayList<Pedidos>();
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(URL, USER, PASS);
			PreparedStatement ps = conn.prepareStatement(
					"Select pedido.Ped_Id, pe.Descripcion, pedido.Ped_UsuarioId, pedido.Ped_State,pedido.Ped_Date , usr.Nombre, usr.Apellido "
							+ "from Pedidos pedido "
							+ "inner join Pedidos_Estados pe on pedido.Ped_state = pe.PedEstado_Id "
							+ "inner join Usuarios usr on pedido.Ped_UsuarioID = usr.ID");
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Pedidos u = new Pedidos();
				u.setId(rs.getInt("Ped_Id"));
				u.setPed_NombreUsuario(rs.getString("Nombre"));
				u.setPed_ApellidoUsuario(rs.getString("Apellido"));
				u.setFechaPedido(rs.getDate("Ped_Date"));
				u.setCodPed_State(rs.getInt("Ped_State"));
				u.setPed_State(rs.getString("Descripcion"));
				int pedID = u.getPed_id();

				PreparedStatement ps2 = conn.prepareStatement(
						"Select prod_id,prod.prod_Nombre,prod.prod_precio,prod.prod_contenido,prod.prod_presentacion,prod.prod_Proovedor,prod.prod_Telefono,prod.prod_Stock,prod.prod_TipoProd,pp.Ped_ProdCant from Pedidos pedido "
								+ " inner join Pedidos_Productos pp on pedido.Ped_Id = pp.IDPedidos  "
								+ " inner join Productos prod on pp.Ped_ProdId = prod.Prod_id and pedido.Ped_ID="
								+ pedID);
				ResultSet rs2 = ps2.executeQuery();

				List listaProductos = new ArrayList<Productos>();
				float total = '0';
				while (rs2.next()) {
					Productos p = new Productos();
					p.setId(rs2.getInt("prod_id"));
					p.setName(rs2.getString("prod_Nombre"));
					p.setProd_Contenido(rs2.getString("prod_contenido"));
					p.setProd_Precio(rs2.getString("prod_precio"));
					p.setProd_Presentacion(rs2.getString("prod_presentacion"));
					p.setProd_Proovedor(rs2.getString("prod_proovedor"));
					p.setProd_Telefono(rs2.getString("prod_telefono"));
					p.setProd_TipoProd(rs2.getInt("prod_TipoProd"));
					p.setProd_CantProd(rs2.getInt("Ped_ProdCant"));
					p.setStock(rs2.getInt("prod_Stock"));
					float pPrecio = Float.parseFloat(rs2.getString("prod_precio"));
					total = total + pPrecio;
					listaProductos.add(p);
				}
				u.setPedido_Producto(listaProductos);
				u.setTotal(total);
				Pedidos.add(u);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return Pedidos;
	}

	public Pedidos VerDetalleDePedido(int id) {
		Pedidos u = new Pedidos();
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(URL, USER, PASS);
			PreparedStatement ps = conn.prepareStatement(
					"Select pedido.Ped_Id, pe.Descripcion, pedido.Ped_UsuarioId, pedido.Ped_State,pedido.Ped_Date,pedido.Ped_Date , usr.Nombre, usr.Apellido "
							+ "from Pedidos pedido"
							+ "inner join Pedidos_Estados pe on pedido.Ped_state = pe.PedEstado_Id "
							+ " inner join Usuarios usr on pedido.Ped_UsuarioID = usr.ID " + "where pedido.Ped_ID ="
							+ id);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {

				u.setId(rs.getInt("Ped_Id"));
				u.setPed_NombreUsuario(rs.getString("Nombre"));
				u.setPed_ApellidoUsuario(rs.getString("Apellido"));
				u.setPed_State(rs.getString("Descripcion"));
				u.setFechaPedido(rs.getDate("Ped_Date"));
				u.setCodPed_State(rs.getInt("Ped_State"));
				int pedID = u.getPed_id();

				PreparedStatement ps2 = conn.prepareStatement(
						"Select prod_id,prod.prod_Nombre,prod.prod_precio,prod.prod_contenido,prod.prod_presentacion,prod.prod_Proovedor,prod.prod_Telefono,prod.prod_Stock,prod.prod_TipoProd,pp.Ped_ProdCant  from Pedidos pedido "
								+ " inner join Pedidos_Productos pp on pedido.Ped_Id = pp.IDPedidos  "
								+ " inner join Productos prod on pp.Ped_ProdId = prod.Prod_id where pedido.Ped_ID ="
								+ pedID);
				ResultSet rs2 = ps2.executeQuery();

				List listaProductos = new ArrayList<Productos>();
				float total = '0';
				while (rs2.next()) {
					Productos p = new Productos();
					p.setId(rs2.getInt("prod_id"));
					p.setName(rs2.getString("prod_Nombre"));
					p.setProd_Contenido(rs2.getString("prod_contenido"));
					p.setProd_Precio(rs2.getString("prod_precio"));
					p.setProd_Presentacion(rs2.getString("prod_presentacion"));
					p.setProd_Proovedor(rs2.getString("prod_proovedor"));
					p.setProd_Telefono(rs2.getString("prod_telefono"));
					p.setProd_TipoProd(rs2.getInt("prod_TipoProd"));
					p.setProd_CantProd(rs2.getInt("Ped_ProdCant"));
					p.setStock(rs2.getInt("prod_Stock"));
					float pPrecio = Float.parseFloat(rs2.getString("prod_precio"));
					total = total + pPrecio;
					listaProductos.add(p);
				}
				u.setPedido_Producto(listaProductos);
				u.setTotal(total);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return u;
	}

	
	public List<Pedidos> BuscarPorFecha(String desde, String hasta) {
		Pedidos u = new Pedidos();
		List<Pedidos> listaPedidos = new ArrayList<Pedidos>();
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(URL, USER, PASS);			
			PreparedStatement ps = conn.prepareStatement(
					"Select pedido.Ped_Id, pe.Descripcion, pedido.Ped_UsuarioId, pedido.Ped_State,pedido.Ped_Date,pedido.Ped_Date , usr.Nombre, usr.Apellido "
							+ "from Pedidos pedido"
							+ "inner join Pedidos_Estados pe on pedido.Ped_state = pe.PedEstado_Id "
							+ "inner join Usuarios usr on pedido.Ped_UsuarioID = usr.ID " 
							+ "where pedido.Ped_Date >=" + desde
							+ "and pedido.Ped_Date <=" + hasta);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {

				u.setId(rs.getInt("Ped_Id"));
				u.setPed_NombreUsuario(rs.getString("Nombre"));
				u.setPed_ApellidoUsuario(rs.getString("Apellido"));
				u.setPed_State(rs.getString("Descripcion"));
				u.setFechaPedido(rs.getDate("Ped_Date"));
				u.setCodPed_State(rs.getInt("Ped_State"));
				int pedID = u.getPed_id();

				PreparedStatement ps2 = conn.prepareStatement(
						"Select prod_id,prod.prod_Nombre,prod.prod_precio,prod.prod_contenido,prod.prod_presentacion,prod.prod_Proovedor,prod.prod_Telefono,prod.prod_Stock,prod.prod_TipoProd,pp.Ped_ProdCant  from Pedidos pedido "
								+ " inner join Pedidos_Productos pp on pedido.Ped_Id = pp.IDPedidos  "
								+ " inner join Productos prod on pp.Ped_ProdId = prod.Prod_id where pedido.Ped_ID ="
								+ pedID);
				ResultSet rs2 = ps2.executeQuery();

				List listaProductos = new ArrayList<Productos>();
				float total = '0';
				while (rs2.next()) {
					Productos p = new Productos();
					p.setId(rs2.getInt("prod_id"));
					p.setName(rs2.getString("prod_Nombre"));
					p.setProd_Contenido(rs2.getString("prod_contenido"));
					p.setProd_Precio(rs2.getString("prod_precio"));
					p.setProd_Presentacion(rs2.getString("prod_presentacion"));
					p.setProd_Proovedor(rs2.getString("prod_proovedor"));
					p.setProd_Telefono(rs2.getString("prod_telefono"));
					p.setProd_TipoProd(rs2.getInt("prod_TipoProd"));
					p.setProd_CantProd(rs2.getInt("Ped_ProdCant"));
					p.setStock(rs2.getInt("prod_Stock"));
					float pPrecio = Float.parseFloat(rs2.getString("prod_precio"));
					total = total + pPrecio;
					listaProductos.add(p);
				}
				u.setPedido_Producto(listaProductos);
				u.setTotal(total);
				listaPedidos.add(u);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return listaPedidos;
	}
	
	
	
	public Pedidos CambiarEstadoEnEspera(int u) {

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(URL, USER, PASS);
			String sql = "Update Pedidos set Ped_State = " + 1 + " where Ped_Id=" + u;
			Statement st = conn.createStatement();
			int can = st.executeUpdate(sql);
			System.out.println("registros actualizados " + can);
		} catch (SQLException e) {

			e.printStackTrace();
		} finally {

			try {
				conn.close();
			} catch (SQLException e) {

				e.printStackTrace();
			}
		}
		return null;
	}

	public Pedidos CambiarEstadoEnCurso(int u) {
		System.out.println(u);
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(URL, USER, PASS);
			String sql = "Update Pedidos set Ped_State = " + 2 + " where Ped_Id=" + u;
			System.out.println(sql);
			Statement st = conn.createStatement();
			int can = st.executeUpdate(sql);

		} catch (SQLException e) {

			e.printStackTrace();
		} finally {

			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;

	}

	public Pedidos CambiarEstadoFinalizado(int u) {

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(URL, USER, PASS);
			String sql = "Update Pedidos set Ped_State = " + 3 + " where Ped_Id=" + u;
			Statement st = conn.createStatement();
			int can = st.executeUpdate(sql);

		} catch (SQLException e) {

			e.printStackTrace();
		} finally {

			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	public Pedidos CambiarEstadoCancelado(int u) {

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(URL, USER, PASS);
			String sql = "Update Pedidos set Ped_State =" + 4 + " where Ped_Id=" + u;
			Statement st = conn.createStatement();
			int can = st.executeUpdate(sql);

		} catch (SQLException e) {

			e.printStackTrace();
		} finally {

			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	public List<Pedidos> VerPedidosPendientes() {
		List<Pedidos> Pedidos = new ArrayList<Pedidos>();
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(URL, USER, PASS);
			PreparedStatement ps = conn.prepareStatement(
					"Select pe.Descripcion, pedido.Ped_Id, pedido.Ped_UsuarioId, pedido.Ped_State,pedido.Ped_Date , usr.Nombre, usr.Apellido "
							+ "from Pedidos pedido "
							+ "inner join Pedidos_Estados pe on pedido.Ped_state = pe.PedEstado_Id "
							+ "inner join Usuarios usr on pedido.Ped_UsuarioID = usr.ID "
							+ "where pedido.Ped_state in (1,2)  ");
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Pedidos u = new Pedidos();
				u.setId(rs.getInt("Ped_Id"));
				u.setPed_NombreUsuario(rs.getString("Nombre"));
				u.setPed_ApellidoUsuario(rs.getString("Apellido"));
				u.setPed_State(rs.getString("Descripcion"));
				u.setFechaPedido(rs.getDate("Ped_Date"));
				u.setCodPed_State(rs.getInt("Ped_State"));
				int pedID = u.getPed_id();

				PreparedStatement ps2 = conn.prepareStatement(
						"Select prod_id, prod.prod_Nombre, prod.prod_precio, prod.prod_contenido, prod.prod_presentacion, prod.prod_Proovedor, prod.prod_Telefono, prod.prod_Stock, prod.prod_TipoProd,pp.Ped_ProdCant  "
								+ "from Pedidos pedido "
								+ "inner join Pedidos_Productos pp on pedido.Ped_Id = pp.IDPedidos "
								+ "inner join Productos prod on pp.Ped_ProdId = prod.Prod_id "
								+ "where pedido.Ped_state in (1,2) and pedido.Ped_ID=" + pedID);
				ResultSet rs2 = ps2.executeQuery();

				List listaProductos = new ArrayList<Productos>();
				float total = '0';
				while (rs2.next()) {
					Productos p = new Productos();
					p.setId(rs2.getInt("prod_id"));
					p.setName(rs2.getString("prod_Nombre"));
					p.setProd_Contenido(rs2.getString("prod_contenido"));
					p.setProd_Precio(rs2.getString("prod_precio"));
					p.setProd_Presentacion(rs2.getString("prod_presentacion"));
					p.setProd_Proovedor(rs2.getString("prod_proovedor"));
					p.setProd_Telefono(rs2.getString("prod_telefono"));
					p.setProd_TipoProd(rs2.getInt("prod_TipoProd"));
					p.setProd_CantProd(rs2.getInt("Ped_ProdCant"));
					p.setStock(rs2.getInt("prod_Stock"));
					float pPrecio = Float.parseFloat(rs2.getString("prod_precio"));
					total = total + pPrecio;
					listaProductos.add(p);
					System.out.println(p.getProd_Id());
				}
				u.setPedido_Producto(listaProductos);
				u.setTotal(total);
				Pedidos.add(u);
				System.out.println(u.getPed_NombreUsuario());

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return Pedidos;
	}

	public List<Pedidos> VerPedidosRealizados() {
		List<Pedidos> Pedidos = new ArrayList<Pedidos>();
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(URL, USER, PASS);
			PreparedStatement ps = conn.prepareStatement(
					"Select pedido.Ped_Id, pedido.Ped_UsuarioId, pedido.Ped_State,pedido.Ped_Date ,usr.Nombre, usr.Apellido, pe.Descripcion "
							+ "from Pedidos pedido "
							+ "inner join Pedidos_Estados pe on pedido.Ped_state = pe.PedEstado_Id "
							+ "inner join Usuarios usr on pedido.Ped_UsuarioID = usr.ID "
							+ "where pedido.Ped_state in (3,4)  ");
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Pedidos u = new Pedidos();
				u.setId(rs.getInt("Ped_Id"));
				u.setPed_NombreUsuario(rs.getString("Nombre"));
				u.setPed_ApellidoUsuario(rs.getString("Apellido"));
				u.setPed_State(rs.getString("Descripcion"));
				u.setFechaPedido(rs.getDate("Ped_Date"));
				u.setCodPed_State(rs.getInt("Ped_State"));
				int pedID = u.getPed_id();
				PreparedStatement ps2 = conn.prepareStatement(
						"Select prod_id,prod.prod_Nombre,prod.prod_precio,prod.prod_contenido,prod.prod_presentacion,prod.prod_Proovedor,prod.prod_Telefono,prod.prod_Stock,prod.prod_TipoProd,pp.Ped_ProdCant  "
								+ "from Pedidos pedido "
								+ "inner join Pedidos_Productos pp on pedido.Ped_Id = pp.IDPedidos "
								+ "inner join Productos prod on pp.Ped_ProdId = prod.Prod_id "
								+ "where pedido.Ped_state in (3,4) and pedido.Ped_ID=" + pedID);
				ResultSet rs2 = ps2.executeQuery();

				List listaProductos = new ArrayList<Productos>();
				float total = '0';
				while (rs2.next()) {
					Productos p = new Productos();
					p.setId(rs2.getInt("prod_id"));
					p.setName(rs2.getString("prod_Nombre"));
					p.setProd_Contenido(rs2.getString("prod_contenido"));
					p.setProd_Precio(rs2.getString("prod_precio"));
					p.setProd_Presentacion(rs2.getString("prod_presentacion"));
					p.setProd_Proovedor(rs2.getString("prod_proovedor"));
					p.setProd_Telefono(rs2.getString("prod_telefono"));
					p.setProd_TipoProd(rs2.getInt("prod_TipoProd"));
					p.setProd_CantProd(rs2.getInt("Ped_ProdCant"));
					p.setStock(rs2.getInt("prod_Stock"));
					float pPrecio = Float.parseFloat(rs2.getString("prod_precio"));
					total = total + pPrecio;
					listaProductos.add(p);
					System.out.println(p.getProd_Id());
				}
				u.setPedido_Producto(listaProductos);
				u.setTotal(total);
				Pedidos.add(u);
				System.out.println(u.getPed_NombreUsuario());

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return Pedidos;
	}
}

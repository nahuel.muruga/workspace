package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.sql.*;

import model.Usuario;
import model.Pedidos_Estados;
import model.Pedidos_Productos;
import model.Pedidos;
import model.Productos;

public class UsuarioDao {
	private Connection comm;
	public static final String Controlador =  "com.mysql.jdbc.Driver"; //"org.apache.derby.jdbc.ClientDriver"; //
	public static final String URL =  "jdbc:mysql://127.0.0.1:3306/proyectofinal_db"; //"jdbc:derby://localhost:1527/login";//
	public static final String USER= "root";
	public static final String PASS= "";
	
	static {
		try {
			Class.forName(Controlador);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public List<Usuario> obtenerTodos() {
		List<Usuario> usuarios = new ArrayList<Usuario>();
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(URL , USER , PASS);
			PreparedStatement ps = conn.prepareStatement("Select * from usuarios");
			
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Usuario u = new Usuario();
				u.setId(rs.getInt("id"));
				u.setUser(rs.getString("usuario"));
				u.setPass(rs.getString("pass"));
				u.setName(rs.getString("nombre"));
				u.setApellido(rs.getString("apellido"));
				u.setdni(rs.getString("dni"));
				u.setEstadoCivil(rs.getInt("estciv"));
				System.out.println("El est Civ es :" +rs.getInt("estciv") );
				// u.setCiudad(rs.getString("ciudad"));
				u.setDomicilio(rs.getString("domicilio"));
				u.setTelefono(rs.getString("telefono"));
				u.setMail(rs.getString("email"));
				u.setAdmin(rs.getBoolean("admin"));
				usuarios.add(u);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			try {
				conn.close();
			} catch (SQLException e) {

				e.printStackTrace();
			}
		}
		return usuarios;
	}

	public Usuario insertar(Usuario u) {

		Connection conn;
		try {
			conn = DriverManager.getConnection(URL , USER , PASS);
			System.out.println("El est Civ es :" +u.getEstadoCivil() );
			PreparedStatement ps = conn.prepareStatement(
					"Insert Into Usuarios (usuario,pass,nombre,apellido,dni,EstCiv,email,telefono,celular,domicilio,admin) VALUES (?,?,?,?,?,?,?,?,?,?,?)",
					PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setString(1, u.getUser());
			ps.setString(2, u.getPass());
			ps.setString(3, u.getName());
			ps.setString(4, u.getApellido());
			ps.setString(5, u.getDni());
			ps.setInt(6, u.getEstadoCivil()); 
			ps.setString(7, u.getDomicilio());
			ps.setString(8, u.getTelefono());
			ps.setString(9, u.getMail());
			ps.setString(10, u.getCiudad());
			ps.setBoolean(11, u.getAdmin());

			List<Usuario> usuario = new ArrayList<Usuario>();
			int cant = ps.executeUpdate();

			ResultSet rs = ps.getGeneratedKeys();
			while (rs.next()) {
				u.setId(rs.getInt(1));

			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return u;

	}

	public Usuario obtenerUno(int id) {

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(URL , USER , PASS);
			PreparedStatement ps = conn.prepareStatement("Select * from usuarios where id =" + id);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				Usuario u = new Usuario();
				u.setId(rs.getInt("id"));
				u.setUser(rs.getString("usuario"));
				u.setPass(rs.getString("pass"));
				u.setName(rs.getString("nombre"));
				u.setApellido(rs.getString("apellido"));
				u.setdni(rs.getString("dni"));
			    u.setEstadoCivil(rs.getInt("estciv"));
				// u.setCiudad(rs.getString("ciudad"));
				// u.setDomicilio(rs.getString("domicilio"));
				u.setTelefono(rs.getString("telefono"));
				u.setMail(rs.getString("email"));
				u.setAdmin(rs.getBoolean("admin"));
				return u;

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			try {
				conn.close();
			} catch (SQLException e) {

				e.printStackTrace();
			}
		}
		return null;
	}

	public Usuario ModificarUno(Usuario u) {
		int administrador;
		if(u.getAdmin()) {
		   administrador = 1 ;
		}else {
			administrador = 0;
		}
		
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(URL , USER , PASS);
			String sql = "Update Usuarios set usuario = '" + u.getUser() + "',pass='" + u.getPass() + "',nombre='"
					+ u.getName() + "',apellido='" + u.getApellido() + "',dni='" + u.getDni() + "',email='"
					+ u.getMail() + "',telefono='" + u.getTelefono() + "',estciv='" + u.getEstadoCivil()
					+ "',domicilio='" + u.getDomicilio() + "',admin='" + administrador + "' where id=" + u.getId();
			Statement st = conn.createStatement();
			int can = st.executeUpdate(sql);
			System.out.println("registros actualizados " + can);
		} catch (SQLException e) {
			System.out.println("error en el update");
			e.printStackTrace();
		} finally {

			try {
				conn.close();
			} catch (SQLException e) {

				e.printStackTrace();
			}
		}
		return null;
	}

	public Usuario EliminarUsuario(Usuario u) {

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(URL , USER , PASS);
			String sql = "Delete from Usuarios where id=" + u.getId();
			Statement st = conn.createStatement();
			int can = st.executeUpdate(sql);

		} catch (SQLException e) {

			e.printStackTrace();
		} finally {

			try {
				conn.close();
			} catch (SQLException e) {

				e.printStackTrace();
			}
		}
		return null;
	}

}

package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.sql.*;
import model.Pedidos_Estados;
import model.Pedidos_Productos;
import model.Pedidos;
import model.Productos;

public class ProductosDao {
	private Connection comm;
	public static final String Controlador =  "com.mysql.jdbc.Driver"; //"org.apache.derby.jdbc.ClientDriver"; //
	public static final String URL =  "jdbc:mysql://127.0.0.1:3306/proyectofinal_db"; //"jdbc:derby://localhost:1527/login";//
	public static final String USER= "root";
	public static final String PASS= "";
	

	static {
		try {
			Class.forName(Controlador);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public List<Productos> obtenerTodos() {
		List<Productos> Productos = new ArrayList<Productos>();
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(URL , USER , PASS);
			PreparedStatement ps = conn.prepareStatement("Select * from Productos");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Productos u = new Productos();
				u.setId(rs.getInt("prod_id"));
				u.setName(rs.getString("prod_Nombre"));
				u.setProd_Contenido(rs.getString("prod_contenido"));
				u.setProd_Precio(rs.getString("prod_precio"));
				u.setProd_Presentacion(rs.getString("prod_presentacion"));
				u.setProd_Proovedor(rs.getString("prod_proovedor"));
				u.setProd_Telefono(rs.getString("prod_telefono"));
				u.setProd_TipoProd(rs.getInt("prod_TipoProd"));
				u.setStock(rs.getInt("prod_Stock"));

				Productos.add(u);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return Productos;
	}

	public Productos insertar(Productos u) {

		Connection conn;
		try {
			conn = DriverManager.getConnection(URL , USER , PASS);

			PreparedStatement ps = conn.prepareStatement(
					"Insert Into Productos (prod_Nombre,prod_precio,prod_contenido,prod_presentacion,prod_Proovedor,prod_Telefono,prod_Stock,prod_TipoProd) VALUES (?,?,?,?,?,?,?,?)",
					PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setString(1, u.getProd_Name());
			ps.setString(2, u.getProd_Precio());
			ps.setString(3, u.getProd_Contenido());
			ps.setString(4, u.getProd_Presentacion());
			ps.setString(5, u.getProd_Proovedor());
			ps.setString(6, u.getProd_Telefono());
			ps.setInt(7, u.getProd_Stock());
			ps.setInt(8, u.getProd_TipoProd());

			List<Productos> Producto = new ArrayList<Productos>();
			int cant = ps.executeUpdate();

			ResultSet rs = ps.getGeneratedKeys();
			while (rs.next()) {
				u.setId(rs.getInt(1));

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return u;

	}

	public Productos EliminarProducto(Productos u) {

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(URL , USER , PASS);
			String sql = "Delete from Productos where prod_id=" + u.getProd_Id();
			Statement st = conn.createStatement();
			int can = st.executeUpdate(sql);

		} catch (SQLException e) {

			e.printStackTrace();
		} finally {

			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	public Productos obtenerUno(int id) {

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(URL , USER , PASS);
			PreparedStatement ps = conn.prepareStatement("Select * from Productos where prod_id =" + id);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				Productos u = new Productos();
				u.setId(rs.getInt("prod_id"));
				u.setName(rs.getString("prod_Nombre"));
				u.setProd_Contenido(rs.getString("prod_contenido"));
				u.setProd_Precio(rs.getString("prod_precio"));
				u.setProd_Presentacion(rs.getString("prod_presentacion"));
				u.setProd_Proovedor(rs.getString("prod_proovedor"));
				u.setProd_Telefono(rs.getString("prod_Telefono"));
				u.setProd_TipoProd(rs.getInt("prod_TipoProd"));
				u.setStock(rs.getInt("prod_Stock"));

				return u;

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	public Productos ModificarUno(Productos u) {

		String prod_precio = u.getProd_Precio();
		float pPrecio = Float.parseFloat(prod_precio);
		System.out.println(pPrecio);

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(URL , USER , PASS);
			String sql = "Update Productos set prod_Nombre = '" + u.getProd_Name() + "',prod_contenido='"
					+ u.getProd_Contenido() + "',prod_precio=" + pPrecio + ",prod_presentacion='"
					+ u.getProd_Presentacion() + "',prod_proovedor='" + u.getProd_Proovedor() + "',prod_Telefono='"
					+ u.getProd_Telefono() + "',prod_Stock=" + u.getProd_Stock() + ",prod_TipoProd="
					+ u.getProd_TipoProd() + " where prod_id=" + u.getProd_Id();
			Statement st = conn.createStatement();
			int can = st.executeUpdate(sql);

		} catch (SQLException e) {

			e.printStackTrace();
		} finally {

			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

}

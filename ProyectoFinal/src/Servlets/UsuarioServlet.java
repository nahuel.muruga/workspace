package Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;
import model.Pedidos_Estados;
import model.Pedidos_Productos;
import model.Pedidos;
import model.Productos;
import model.Usuario;
import dao.UsuarioDao;

public class UsuarioServlet extends HttpServlet implements Servlet {
	private static final long serialVersionUID = 1L;
	private UsuarioDao dao;

	public UsuarioServlet() {
		dao = new UsuarioDao();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String id = request.getParameter("id"); // recupero ID por que si no, no puedo completar la grilla de
												// Usuarios.jsp
		String accion = request.getParameter("accion");
		RequestDispatcher rd;
		PrintWriter out = response.getWriter();

		if (accion.equals("EditarUsuario")) {
			int Intid = Integer.parseInt(id);
			Usuario user = dao.obtenerUno(Intid);
			rd = request.getRequestDispatcher("/NuevoUsuario.jsp");
			request.setAttribute("Usuario", user);
			rd.forward(request, response);
		} else if (accion.equals("NuevoUsuario")) {
			Usuario u = new Usuario();
			System.out.println("El est Civ es :" +u.getEstadoCivil() );
			request.setAttribute("Usuario", u);
			rd = request.getRequestDispatcher("/NuevoUsuario.jsp");
			rd.forward(request, response);
		} else if (accion.equals("EliminarUsuario")) {
			int ProdId = Integer.parseInt(id);
			Usuario user = new Usuario();
			user.setId(ProdId);
			System.out.println("Pasa por el Eliminar");
			dao.EliminarUsuario(user);
			mostrarUsuario(request, response);
		} else if (accion.equals("usuario")) {
			mostrarUsuario(request, response);
		}
	}

	private void mostrarUsuario(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<Usuario> usuarios = dao.obtenerTodos();
		request.setAttribute("usuarios", usuarios);
		RequestDispatcher rd = request.getRequestDispatcher("/usuarios.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("textId");
		String admin = request.getParameter("admin");
		String estadoCivil = request.getParameter("selectEstadoCivil");
		int EstCiv = Integer.parseInt(estadoCivil);
		
		Boolean BoolAdmin = false;
		if (admin != null && admin.equals("on")) {
			BoolAdmin = true;
		}

		System.out.println(BoolAdmin);

		Usuario NuevoUser = new Usuario();

		NuevoUser.setName(request.getParameter("textNombres"));
		NuevoUser.setApellido(request.getParameter("txtApellidos"));
		NuevoUser.setdni(request.getParameter("txtDNI"));
		NuevoUser.setEstadoCivil(EstCiv);
		NuevoUser.setCiudad(request.getParameter("ciudad"));
		NuevoUser.setDomicilio(request.getParameter("domicilio"));
		NuevoUser.setTelefono(request.getParameter("telefono"));
		NuevoUser.setMail(request.getParameter("email"));
		NuevoUser.setUser(request.getParameter("usuario"));
		NuevoUser.setPass(request.getParameter("pass"));
		NuevoUser.setAdmin(BoolAdmin);

		if (null == id) {
			dao.insertar(NuevoUser);
			mostrarUsuario(request, response);
			// get after post.
		} else {
			int Intid = Integer.parseInt(id);
			NuevoUser.setId(Intid);
			dao.ModificarUno(NuevoUser);
			mostrarUsuario(request, response);

		}

	}

}

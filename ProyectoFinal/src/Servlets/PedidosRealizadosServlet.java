package Servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.PedidosDao;
import model.Pedidos_Estados;
import model.Pedidos_Productos;
import model.Pedidos;
import model.Productos;


public class PedidosRealizadosServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private PedidosDao dao;


	public PedidosRealizadosServlet() {
		dao = new PedidosDao();
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String id = request.getParameter("id"); // recupero ID por que si no, no puedo completar la grilla de
												// Usuarios.jsp
		String accion = request.getParameter("accion");
		RequestDispatcher rd;

		if (accion.equals("VerDetalle")) {
			int Intid = Integer.parseInt(id);
			Pedidos pedido = dao.VerDetalleDePedido(Intid);
			rd = request.getRequestDispatcher("/VerDetallePedido.jsp");
			request.setAttribute("Pedido", pedido);
			rd.forward(request, response);
		} else {
			mostrarPedido(request, response);
		}
	}

	private void mostrarPedido(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<Pedidos> Pedido = dao.VerPedidosRealizados();
		// System.out.println(Listaproducto.get(0));
		System.out.println("Pasa por el servlet");
		request.setAttribute("Pedido", Pedido);
		RequestDispatcher rd = request.getRequestDispatcher("/prealizados.jsp");
		rd.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String id = request.getParameter("textId");
		String TipoPed = request.getParameter("prod_TipoProd");
		String Stock = request.getParameter("prod_Stock");
		int tp = Integer.parseInt(TipoPed);
		int stk = Integer.parseInt(Stock);

		Pedidos NuevoPed = new Pedidos();
		Pedidos_Productos PedidoPed = new Pedidos_Productos();
		Pedidos_Estados PedidoEst = new Pedidos_Estados();

		mostrarPedido(request, response);

	}

}

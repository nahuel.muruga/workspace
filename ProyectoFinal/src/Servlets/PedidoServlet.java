package Servlets;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.csvreader.CsvWriter;

import dao.PedidosDao;
import model.Pedidos;
import model.Pedidos_Estados;
import model.Pedidos_Productos;

public class PedidoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private PedidosDao dao;
	public String Sdesde;
	public String Shasta;

	public PedidoServlet() {
		dao = new PedidosDao();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String id = request.getParameter("id"); // recupero ID por que si no, no puedo completar la grilla de
												// Usuarios.jsp
		String accion = request.getParameter("accion");

		RequestDispatcher rd;

		if (accion.equals("CambiarEstadoEnCurso")) {
			int Intid = Integer.parseInt(id);
			Pedidos pedido = dao.CambiarEstadoEnCurso(Intid);
			rd = request.getRequestDispatcher("/ppendientes.jsp");
			request.setAttribute("Pedido", pedido);
			rd.forward(request, response);
		} else if (accion.equals("Buscar")) {
			// int Intid = Integer.parseInt(id);
			List<Pedidos> pedido = dao.BuscarPorFecha(Sdesde, Shasta);
			rd = request.getRequestDispatcher("/ppendientes.jsp");
			request.setAttribute("Pedido", pedido);
			rd.forward(request, response);
		} else if (accion.equals("CambiarEstadoEnEspera")) {
			int Intid = Integer.parseInt(id);
			Pedidos pedido = dao.CambiarEstadoEnEspera(Intid);
			rd = request.getRequestDispatcher("/ppendientes.jsp");
			request.setAttribute("Pedido", pedido);
			rd.forward(request, response);
		} else if (accion.equals("CambiarEstadoFinalizado")) {
			int Intid = Integer.parseInt(id);
			Pedidos pedido = dao.CambiarEstadoFinalizado(Intid);
			rd = request.getRequestDispatcher("/ppendientes.jsp");
			request.setAttribute("Pedido", pedido);
			rd.forward(request, response);
		} else if (accion.equals("CambiarEstadoCancelado")) {
			int Intid = Integer.parseInt(id);
			Pedidos pedido = dao.CambiarEstadoCancelado(Intid);
			rd = request.getRequestDispatcher("/ppendientes.jsp");
			request.setAttribute("Pedido", pedido);
			rd.forward(request, response);
		} else if (accion.equals("VerDetalle")) {
			int Intid = Integer.parseInt(id);
			Pedidos pedido = dao.VerDetalleDePedido(Intid);
			rd = request.getRequestDispatcher("/VerDetallePedido.jsp");
			request.setAttribute("Pedido", pedido);
			rd.forward(request, response);
		} else if (accion.equals("ExportarCSV")) {
			List<Pedidos> pedido = dao.VerPedidosPendientes();

			String[] columnsName = { "ID", "Nombre", "Apellido", "Estado", "Total" };
			Workbook workbook = new XSSFWorkbook();

			CreationHelper createHelper = workbook.getCreationHelper();

			Sheet sheet = (Sheet) workbook.createSheet("Employee");

			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setFontHeightInPoints((short) 12);
			headerFont.setColor(IndexedColors.BLACK.getIndex());

			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);

			Row headerRow = (Row) sheet.createRow(0);

			for (int i = 0; i < columnsName.length; i++) {
				Cell cell = headerRow.createCell(i);
				cell.setCellValue(columnsName[i]);
				cell.setCellStyle(headerCellStyle);
			}

			List<Pedidos> pedidos = (List<Pedidos>) pedido;
			CellStyle dateCellStyle = workbook.createCellStyle();
			dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

			int rowNum = 1;
			for (Pedidos pedidoActual : pedidos) {
				Row row = sheet.createRow(rowNum++);

				row.createCell(0).setCellValue(pedidoActual.getPed_id());

				row.createCell(1).setCellValue(pedidoActual.getPed_NombreUsuario());

				row.createCell(2).setCellValue(pedidoActual.getPed_ApellidoUsuario());

				row.createCell(3).setCellValue(pedidoActual.getPed_State());

				row.createCell(4).setCellValue(pedidoActual.getTotal());

				/*
				 * Cell dateOfBirthCell = row.createCell(2);
				 * dateOfBirthCell.setCellValue(pedidos.getDateOfBirth());
				 * dateOfBirthCell.setCellStyle(dateCellStyle);
				 */

			}

			for (int i = 0; i < columnsName.length; i++) {
				sheet.autoSizeColumn(i);
			}

			LocalDateTime ldt = LocalDateTime.now();
			DateTimeFormatter formmat1 = DateTimeFormatter.ofPattern("dd-MM-yyyy", Locale.ENGLISH);
			String formatter = formmat1.format(ldt);
			FileOutputStream fileOut = new FileOutputStream("c:/temp/Reporte_" + formatter + ".xlsx");
			workbook.write(fileOut);
			fileOut.close();

			workbook.close();

			rd = request.getRequestDispatcher("/ppendientes.jsp");
			request.setAttribute("Pedido", pedido);
			rd.forward(request, response);

		} else {
			mostrarPedido(request, response);
		}
	}

	private void mostrarPedido(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<Pedidos> Pedido = dao.VerPedidosPendientes();
		System.out.println("Pasa por el servlet");
		request.setAttribute("Pedido", Pedido);
		RequestDispatcher rd = request.getRequestDispatcher("/ppendientes.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Sdesde = request.getParameter("fecDesde");
		Shasta = request.getParameter("fecHasta");
		String accion = request.getParameter("accion");

		RequestDispatcher rd;

		String id = request.getParameter("textId");
		String TipoPed = request.getParameter("prod_TipoProd");
		String Stock = request.getParameter("prod_Stock");
		int tp = Integer.parseInt(TipoPed);
		int stk = Integer.parseInt(Stock);

		Pedidos NuevoPed = new Pedidos();
		Pedidos_Productos PedidoPed = new Pedidos_Productos();
		Pedidos_Estados PedidoEst = new Pedidos_Estados();

		mostrarPedido(request, response);
	}

}

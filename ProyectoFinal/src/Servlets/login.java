package Servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Pedidos_Estados;
import model.Pedidos_Productos;
import model.Pedidos;
import model.Productos;

/**
 * Servlet implementation class login
 */
public class login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String Controlador =  "com.mysql.jdbc.Driver"; //"org.apache.derby.jdbc.ClientDriver"; //
	public static final String URL =  "jdbc:mysql://127.0.0.1:3306/proyectofinal_db"; //"jdbc:derby://localhost:1527/login";//
	public static final String USER= "root";
	public static final String PASS= "";
	
	public static Boolean esAdmin = false;

	public login() throws ClassNotFoundException {
		Class.forName(Controlador);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String usuario = request.getParameter("user");
		String clave = request.getParameter("password");

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(URL,USER,PASS);
			String sql = "Select admin from usuarios where USUARIO='" + usuario + "'AND PASS='" + clave + "'";
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) {

				boolean Admin = rs.getBoolean("admin");
				esAdmin = Admin;

				RequestDispatcher rd = request.getRequestDispatcher("/menu.jsp");
				rd.forward(request, response);
				return;

			}
			RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
			request.setAttribute("error", "Usuario o clave incorrecto");
			rd.forward(request, response);
			conn.close();

		} catch (Exception e) {

			e.printStackTrace();

		}

	}
}
package Servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class menu extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public menu() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String accion = request.getParameter("accion");
		RequestDispatcher rd;

		if (accion.equals("usuario")) {
			rd = request.getRequestDispatcher("/UsuarioServlet");
			rd.forward(request, response);
		} else if (accion.equals("producto")) {
			rd = request.getRequestDispatcher("/ProductoServlet");
			rd.forward(request, response);
		} else if (accion.equals("ppendientes")) {
			rd = request.getRequestDispatcher("/PedidoServlet");
			rd.forward(request, response);
		} else if (accion.equals("pprealizados")) {
			rd = request.getRequestDispatcher("/PedidosRealizadosServlet");
			rd.forward(request, response);

		} else if (accion.equals("menu")) {
			rd = request.getRequestDispatcher("/menu.jsp");
			rd.forward(request, response);
		} else if (accion.equals("cerrarSesion")) {
			rd = request.getRequestDispatcher("/index.jsp");
			rd.forward(request, response);
		} else {
			rd = request.getRequestDispatcher("/menu.jsp");
			rd.forward(request, response);
		}

	}

}

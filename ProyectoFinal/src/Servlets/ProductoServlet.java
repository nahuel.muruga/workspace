package Servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ProductosDao;
import model.Pedidos_Estados;
import model.Pedidos_Productos;
import model.Pedidos;
import model.Productos;

/**
 * Servlet implementation class ProductoServlet
 */
public class ProductoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private ProductosDao dao;
		
	  

	
    public ProductoServlet() {
        dao = new ProductosDao();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");	//recupero ID por que si no, no puedo completar la grilla de Usuarios.jsp
		String accion = request.getParameter("accion");
		RequestDispatcher rd;
		 

			if (accion.equals("EditarProducto")) {
				int Intid = Integer.parseInt(id);
				Productos prod = dao.obtenerUno(Intid) ;
				rd = request.getRequestDispatcher("/NuevoProducto.jsp");
				request.setAttribute("Producto",prod );
				rd.forward(request, response);
			} else if (accion.equals("NuevoProducto")){
				Productos prod = new Productos();
				request.setAttribute("Producto", prod);
				rd = request.getRequestDispatcher("/NuevoProducto.jsp");
				rd.forward(request, response);	
			} else if (accion.equals("EliminarProducto")){
				int ProdId = Integer.parseInt(id);
				Productos prod = new Productos();
				prod.setId(ProdId);
				 System.out.println("Pasa por el Eliminar");
				 System.out.println();
				dao.EliminarProducto(prod);
				mostrarProducto(request, response);
			} else if (accion.equals("producto")){
			mostrarProducto(request, response);
		}
		}



		private void mostrarProducto(HttpServletRequest request,
				HttpServletResponse response) throws ServletException, IOException {
			List<Productos> Listaproducto = dao.obtenerTodos();
			 //System.out.println(Listaproducto.get(0));
			 System.out.println("Pasa por el servlet");
			request.setAttribute("Producto", Listaproducto);
			RequestDispatcher rd = request.getRequestDispatcher("/productos.jsp");
			rd.forward(request, response);
		}
		

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String id = request.getParameter("textId");
		String TipoProd = request.getParameter("prod_TipoProd");
		String Stock = request.getParameter("prod_Stock");
		
		System.out.println(request.getParameter("prod_Nombre"));
		
		int tp = Integer.parseInt(TipoProd);
		int stk = Integer.parseInt(Stock);
		
		
		Productos NuevoProd = new Productos();
			
	
		NuevoProd.setName(request.getParameter("prod_Nombre")) ;
		NuevoProd.setProd_Contenido(request.getParameter("prod_Contenido")) ;
		NuevoProd.setProd_Precio(request.getParameter("prod_Precio"));
		NuevoProd.setProd_Presentacion(request.getParameter("prod_Presentacion"));
		NuevoProd.setProd_Proovedor(request.getParameter("prod_Proovedor"));
		NuevoProd.setProd_Telefono(request.getParameter("prod_Telefono"));
		NuevoProd.setProd_TipoProd(tp);
		NuevoProd.setStock(stk);
			
		
	
		
		if (null == id)
		{
			dao.insertar(NuevoProd);
			mostrarProducto(request, response);	
			// get after post.
		} else {
			int Intid = Integer.parseInt(id);
			NuevoProd.setId(Intid);
			dao.ModificarUno(NuevoProd);
			mostrarProducto(request, response);
			
		}
		
	}

}



package model;

import java.util.Date;
import java.util.List;

public class Pedidos {

	private int Ped_Id;

	private String Ped_NombreUsuario;

	private String Ped_ApellidoUsuario;

	private String Ped_State;
	
	private int CodPed_State;

	private Float Total;
	
	private Date FechaPedido;

	private List<Productos> Producto;

	public int getPed_id() {
		return Ped_Id;
	}

	public String getPed_NombreUsuario() {
		return Ped_NombreUsuario;
	}

	public String getPed_State() {
		return Ped_State;
	}

	public void setId(int Ped_id) {
		this.Ped_Id = Ped_id;
	}

	public void setPed_NombreUsuario(String Ped_UsId) {
		this.Ped_NombreUsuario = Ped_UsId;
	}

	public void setPed_State(String Prod_Presentacion) {
		this.Ped_State = Prod_Presentacion;
	}

	public List<Productos> getPedido_Producto() {
		return Producto;
	}

	public void setPedido_Producto(List<Productos> pedido_Producto) {
		Producto = pedido_Producto;
	}

	public String getPed_ApellidoUsuario() {
		return Ped_ApellidoUsuario;
	}

	public void setPed_ApellidoUsuario(String ped_ApellidoUsuario) {
		Ped_ApellidoUsuario = ped_ApellidoUsuario;
	}

	public Float getTotal() {
		return Total;
	}

	public void setTotal(Float total) {
		Total = total;
	}

	public int getCodPed_State() {
		return CodPed_State;
	}

	public void setCodPed_State(int codPed_State) {
		CodPed_State = codPed_State;
	}

	public Date getFechaPedido() {
		return FechaPedido;
	}

	public void setFechaPedido(Date fechaPedido) {
		FechaPedido = fechaPedido;
	}


}
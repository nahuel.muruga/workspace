package model;

public class Productos {

	private int Prod_id;
	
	private String Prod_Nombre;
	
	private int Prod_TipoProd;
	
	private int Prod_CantProd;

	private String Prod_Presentacion; 
	
	private String Prod_Contenido;

	private String Prod_Precio;
	
	private String Prod_Proovedor;
	
    private String Prod_Telefono;
	
    private int Prod_Stock;





	
	public int getProd_Id() {
		return Prod_id;
	}
	public int getProd_Stock() {
		return Prod_Stock;
	}

	
	public String getProd_Name() {
		return Prod_Nombre;
	}
	
	public int getProd_TipoProd() {
		return Prod_TipoProd;
	}

	public String getProd_Presentacion() {
		return Prod_Presentacion;
	}
	
	
	public String getProd_Contenido() {
		return Prod_Contenido;
	}
    
	public String getProd_Precio() {
		return Prod_Precio;
	}
	
	public String getProd_Proovedor() {
		return Prod_Proovedor;
	}
	
	public String getProd_Telefono() {
		return Prod_Telefono;
	}

	
	
	public void setId(int id) {
		this.Prod_id = id;
	}
	
	public void setStock(int stock) {
		this.Prod_Stock = stock;
	}
	
	
	
	public void setName(String Prod_Nombre) {
		this.Prod_Nombre = Prod_Nombre;
	}
	
	public void setProd_TipoProd(int Prod_TipoProd) {
		this.Prod_TipoProd = Prod_TipoProd;
	}

	public void setProd_Presentacion(String Prod_Presentacion) {
		this.Prod_Presentacion = Prod_Presentacion;
	}
	
	
	public void setProd_Contenido(String Prod_Contenido) {
		this.Prod_Contenido = Prod_Contenido;
	}
    
	public void setProd_Precio(String Prod_Precio) {
		this.Prod_Precio = Prod_Precio;
	}
	
	public void setProd_Proovedor(String Prod_Proovedor) {
		this.Prod_Proovedor = Prod_Proovedor;
	}
	
	public void setProd_Telefono(String Prod_Telefono) {
		this.Prod_Telefono = Prod_Telefono;
	}
	public int getProd_CantProd() {
		return Prod_CantProd;
	}
	public void setProd_CantProd(int prod_CantProd) {
		Prod_CantProd = prod_CantProd;
	}
		
	public String [] getArray() {
		String[] CamposProductos= {String.valueOf(Prod_id),Prod_Nombre,String.valueOf(Prod_TipoProd),String.valueOf(Prod_CantProd), Prod_Presentacion,Prod_Contenido,Prod_Precio,Prod_Proovedor,Prod_Telefono,String.valueOf(Prod_Stock) };
		return CamposProductos;
	}
	
}

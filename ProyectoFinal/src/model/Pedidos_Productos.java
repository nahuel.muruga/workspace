package model;

public class Pedidos_Productos {
	private int Ped_Id;
	private int Id_Producto ;
	private int Prod_Cant;
	
	public int getPed_id() {
		return Ped_Id;
	}
	
	public int getId_Prod() {
		return Id_Producto;
	}
	
	public int getProd_Cant() {
		return Prod_Cant;
	}
	
	public void setId(int Ped_id) {
		this.Ped_Id = Ped_id;
	}
	
	public void setProd_Cant(int Ped_id) {
		this.Prod_Cant = Ped_id;
	}
	
	public void setId_Producto(int Ped_id) {
		this.Id_Producto = Ped_id;
	}
	
}

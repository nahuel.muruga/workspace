package model;

public class Usuario {

	private int id;
	
	private String user; 
	
	private String pass;
	
	private String nombre;
	
	private String apellido;

	private String dni;
	
	private int estciv;

	private String ciudad;
	
	private String domicilio;
	
    private String telefono;
	
	private String email;

	private boolean Admin;





	
	public boolean getAdmin() {
		return Admin;
	}
	
	public int getId() {
		return id;
	}
	
	public String getUser() {
		return user;
	}
    
	public String getPass() {
		return pass;
	}
	
	public String getName() {
		return nombre;
	}
	
	public String getApellido() {
		return apellido;
	}

	public String getDni() {
		return dni;
	}
	
	
	public int getEstadoCivil() {
		return estciv;
	}
    
	public String getCiudad() {
		return ciudad;
	}
	
	public String getDomicilio() {
		return domicilio;
	}
	
	public String getTelefono() {
		return telefono;
	}

	public String getMail() {
		return email;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setAdmin(boolean admin) {
		this.Admin = admin;
	}
	
	public void setUser(String user) {
		this.user = user;
	}


	public void setPass(String pass) {
		this.pass = pass;
	}
	
	public void setName(String nombre) {
		this.nombre = nombre;
	}
	
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public void setdni(String dni) {
		this.dni = dni;
	}
	
	
	public void setEstadoCivil(int estciv) {
		this.estciv = estciv;
	}
    
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	
	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public void setMail(String email) {
		this.email = email;
	}

	
}

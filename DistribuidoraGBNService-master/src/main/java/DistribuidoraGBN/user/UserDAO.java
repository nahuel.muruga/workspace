package DistribuidoraGBN.user;

import DistribuidoraGBN.base.BaseDAO;

import java.sql.*;

class UserDAO extends BaseDAO {

    User getUser(String user) {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(DATABASE_URL);
            PreparedStatement ps = conn
                    .prepareStatement("Select * from USUARIOS where usuario = '" + user + "'");
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                User u = new User();
                u.setId(rs.getInt("id"));
                u.setUser(rs.getString("usuario"));
                u.setPass(rs.getString("pass"));
                u.setName(rs.getString("nombre"));
                u.setLastName(rs.getString("apellido"));
                u.setMail(rs.getString("email"));
                u.setAdmin(rs.getBoolean("admin"));
                return u;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if(conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}

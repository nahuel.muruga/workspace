package DistribuidoraGBN.user;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @RequestMapping(method = RequestMethod.GET, value = "/user/{user}")
    User user(@PathVariable("user") String user) {
        return new UserDAO().getUser(user);
    }

}

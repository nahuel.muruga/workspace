package DistribuidoraGBN.product;

import DistribuidoraGBN.base.BaseDAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

class ProductDAO extends BaseDAO {

    List<String> getProductTypes() {
        List<String> productTypes = new ArrayList<>();
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(DATABASE_URL);
            PreparedStatement ps = conn.prepareStatement("Select * from Productos_tipos");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                productTypes.add(rs.getString("tipoped_descripcion"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {

            try {
                if(conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return productTypes;
    }

    List<Product> getProductsOfType(String type) {
        List<Product> products = new ArrayList<>();
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(DATABASE_URL);
            PreparedStatement ps = conn.prepareStatement("Select * from Productos " +
                    "INNER JOIN Productos_tipos ON Productos.Prod_TipoProd = Productos_tipos.ped_id " +
                    "WHERE tipoped_descripcion = '" + type + "'");

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Product u = new Product();
                u.setId(rs.getInt("prod_id"));
                u.setName(rs.getString("prod_Nombre"));
                u.setPrice(rs.getFloat("prod_precio"));
                u.setProductType(rs.getString("tipoped_descripcion"));
                u.setStock(rs.getInt("prod_Stock"));
                products.add(u);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {

            try {
                if(conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return products;
    }
}

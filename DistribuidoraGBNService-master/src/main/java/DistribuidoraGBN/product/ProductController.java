package DistribuidoraGBN.product;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {


    @RequestMapping(method = RequestMethod.GET, value = "/product/types")
    List<String> getProductTypes() {
        return new ProductDAO().getProductTypes();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/product/types/{type}")
    List<Product> getProductsOfType(@PathVariable("type") String type) {
        return new ProductDAO().getProductsOfType(type);
    }
}

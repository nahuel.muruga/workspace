package DistribuidoraGBN.purchase;

import DistribuidoraGBN.product.Product;

import java.util.ArrayList;
import java.util.List;

public class Purchase {

    private int purchaseId;
    private int userId;
    private List<Product> productList = new ArrayList<>();
    private String state;

    public int getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(int purchaseId) {
        this.purchaseId = purchaseId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

}

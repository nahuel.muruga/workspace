package DistribuidoraGBN.purchase;

import org.springframework.web.bind.annotation.*;


@RestController
public class PurchaseController {

    @RequestMapping(method = RequestMethod.GET, value = "/purchase/{state}/user/{user}")
    Purchase getPurchaseWithState(@PathVariable("state") String state, @PathVariable("user") String user) {
        return new PurchaseDAO().getPurchaseWithState(state, user);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/purchase/save")
    String getPurchaseWithState(@RequestBody() Purchase purchase) {
        int purchaseId = new PurchaseDAO().savePurchase(purchase);
        return "Pedido " + purchaseId + " generado";
    }
}

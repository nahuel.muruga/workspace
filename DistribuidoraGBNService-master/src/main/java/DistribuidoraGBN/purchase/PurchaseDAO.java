package DistribuidoraGBN.purchase;

import DistribuidoraGBN.base.BaseDAO;
import DistribuidoraGBN.product.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PurchaseDAO extends BaseDAO {

    Purchase getPurchaseWithState(String state, String user) {
        Purchase purchase = new Purchase();
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(DATABASE_URL);
            PreparedStatement ps = conn.prepareStatement("select pe.Descripcion, pedido.Ped_Id, pedido.Ped_UsuarioId, pedido.Ped_State, usr.ID usrId, usr.Usuario "
                    + "from Pedidos pedido "
                    + "inner join Pedidos_Estados pe on pedido.Ped_state = pe.PedEstado_Id "
                    + "inner join Usuarios usr on pedido.Ped_UsuarioID = usr.ID "
                    + "where pe.descripcion = '" + state + "' AND usr.Usuario = '" + user + "'");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                purchase.setPurchaseId(rs.getInt("Ped_Id"));
                purchase.setUserId(rs.getInt("usrId"));
                purchase.setState(rs.getString("Descripcion"));
                int pedID = purchase.getPurchaseId();
                PreparedStatement psProducts = conn.prepareStatement("select prod_id, prod.prod_Nombre, prod.prod_precio, prod.prod_contenido, prod.prod_presentacion, prod.prod_Proovedor, prod.prod_Telefono, prod.prod_Stock, prod.prod_TipoProd, prodTipo.tipoped_descripcion, pp.ped_prodcant "
                        + "from Pedidos pedido "
                        + "inner join Pedidos_Productos pp on pedido.Ped_Id = pp.IDPedidos "
                        + "inner join Productos prod on pp.Ped_ProdId = prod.Prod_id "
                        + "inner join Productos_tipos prodTipo ON prod.Prod_TipoProd = prodTipo.ped_id "
                        + "where pedido.Ped_ID=" + pedID);
                ResultSet rsProducts = psProducts.executeQuery();

                List<Product> listaProductos = new ArrayList<>();
                while (rsProducts.next()){
                    Product p = new Product();
                    p.setId(rsProducts.getInt("prod_id"));
                    p.setName(rsProducts.getString("prod_Nombre"));
                    p.setPrice(rsProducts.getFloat("prod_precio"));
                    p.setStock(rsProducts.getInt("prod_Stock"));
                    p.setProductType(rsProducts.getString("tipoped_descripcion"));
                    p.setQuantity(rsProducts.getInt("ped_prodcant"));
                    listaProductos.add(p);
                }
                purchase.setProductList(listaProductos);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if(conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return purchase;
    }

    int savePurchase(Purchase purchase) {
        int userId = purchase.getUserId();
        int generatedPurchaseId = 0;
        List<Product> productList = purchase.getProductList();
        PreparedStatement preparedStatementPedido = null;
        PreparedStatement preparedStatementLineas = null;
        String insertPurchase = "INSERT INTO pedidos (ped_usuarioid, ped_state) " +
                "VALUES (?, ?)";
        String insertPurchaseProducts = "INSERT INTO pedidos_productos (ped_prodid, ped_prodcant, idpedidos) " +
                "VALUES (?,?,?)";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(DATABASE_URL);
            conn.setAutoCommit(false);
            preparedStatementPedido = conn.prepareStatement(insertPurchase, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatementPedido.setInt(1, userId);
            preparedStatementPedido.setInt(2, 1);
            preparedStatementPedido.execute();
            ResultSet rs = preparedStatementPedido.getGeneratedKeys();
            rs.next();
            generatedPurchaseId = rs.getInt(1);
            for (Product p : productList) {
                preparedStatementLineas = conn.prepareStatement(insertPurchaseProducts);
                preparedStatementLineas.setInt(1, p.getId());
                preparedStatementLineas.setInt(2, p.getQuantity());
                preparedStatementLineas.setInt(3, generatedPurchaseId);
                preparedStatementLineas.execute();
            }
            conn.commit();
            conn.setAutoCommit(true);
        } catch (SQLException e) {
            try {
                if(conn != null) {
                    conn.rollback();
                }
            } catch (SQLException e1) {
                e.printStackTrace();
            }
        } finally {
            try {
                if (preparedStatementPedido != null) preparedStatementPedido.close();
                if (preparedStatementLineas != null) preparedStatementLineas.close();
                if(conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return generatedPurchaseId;
    }


}

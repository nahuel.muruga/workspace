package DistribuidoraGBN.base;

public class BaseDAO {

    protected static final String DATABASE_URL = "jdbc:mysql://127.0.0.1:3306/proyectofinal_db";

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}

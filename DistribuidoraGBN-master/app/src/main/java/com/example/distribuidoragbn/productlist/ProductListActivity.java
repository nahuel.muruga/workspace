package com.example.distribuidoragbn.productlist;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.distribuidoragbn.R;
import com.example.distribuidoragbn.base.BaseActivity;
import com.example.distribuidoragbn.productlist.model.Product;
import com.example.distribuidoragbn.productlist.model.ProductDao;
import com.example.distribuidoragbn.purchase.PurchaseActivity;
import com.example.distribuidoragbn.purchase.model.ActualPurchase;

public class ProductListActivity extends BaseActivity implements AdapterView.OnItemSelectedListener {

    private Spinner productTypeSpinner;
    private Spinner productSpinner;
    private ProductDao productDao;
    private Button addButton;
    private Button finishButton;
    private Button cancelButton;
    private EditText quantityEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        productTypeSpinner = findViewById(R.id.spinner_product_type);
        productDao = new ProductDao();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, productDao.getProductTypes());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        productTypeSpinner.setAdapter(adapter);
        productTypeSpinner.setOnItemSelectedListener(this);

        productSpinner = findViewById(R.id.spinner_product);

        quantityEditText = findViewById(R.id.et_quantity);

        addButton = findViewById(R.id.btn_add_product);
        addButton.setOnClickListener(event -> {
            Product product = (Product) productSpinner.getAdapter().getItem(productSpinner.getSelectedItemPosition());
            String quantityString = quantityEditText.getText().toString();
            if(!quantityString.isEmpty()) {
                int quantity = Integer.valueOf(quantityString);
                if(quantity > 0) {
                    product.setQuantity(quantity);
                    Product existingProduct = ActualPurchase.getInstance().getProductById(product.getId());
                    if(existingProduct == null) {
                        if(quantity <= product.getStock()) {
                            ActualPurchase.getInstance().addProduct(product);
                            showMessageWithTitle("Producto agregado al pedido", "Éxito");
                            quantityEditText.getText().clear();
                        } else {
                            showMessageWithTitle("Sólo hay " + product.getStock() + " unidad(es) de este producto.", "Error");
                        }
                    } else {
                        if(quantity + existingProduct.getQuantity() <= product.getStock()) {
                            ActualPurchase.getInstance().addProduct(product);
                            showMessageWithTitle("Producto agregado al pedido", "Éxito");
                            quantityEditText.getText().clear();
                        } else {
                            showMessageWithTitle("Sólo hay " + product.getStock() + " unidad(es) de este producto.", "Error");
                        }
                    }
                } else {
                    showMessageWithTitle("Ingrese una cantidad mayor a 0", "Error");
                }
            } else {
                showMessageWithTitle("Ingrese una cantidad mayor a 0", "Error");
            }
        });
        finishButton = findViewById(R.id.btn_go_to_purchase);
        finishButton.setOnClickListener(event -> {
            goToActivity(new PurchaseActivity());
        });
        cancelButton = findViewById(R.id.btn_cancel);
        cancelButton.setOnClickListener(event -> finish());
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        ArrayAdapter<Product> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, productDao.getProductsOfType(parent.getItemAtPosition(position).toString()));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        productSpinner.setAdapter(adapter);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}

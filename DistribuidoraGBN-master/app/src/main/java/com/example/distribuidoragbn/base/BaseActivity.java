package com.example.distribuidoragbn.base;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.distribuidoragbn.login.model.User;

public abstract class BaseActivity extends AppCompatActivity {

    private final String SHARED_PREF_NAME = "DistribuidoraSharedPrefs";
    private final String USER_NAME_SHARED_PREF = "UserNameSharedPrefs";
    private final String USER_MAIL_SHARED_PREF = "UserMailSharedPrefs";
    private final String USER_ID_SHARED_PREF = "UserIdSharedPrefs";
    private final String USER_FIRST_NAME_SHARED_PREF = "UserFirstNameSharedPrefs";
    private final String USER_LAST_NAME_SHARED_PREF = "UserLastNameSharedPrefs";

    public void goToActivity(BaseActivity targetActivity) {
        Intent intent = new Intent(this, targetActivity.getClass());
        startActivity(intent);
        finish();
    }

    public void goToActivityNoFinish(BaseActivity targetActivity) {
        Intent intent = new Intent(this, targetActivity.getClass());
        startActivity(intent);
    }

    public void setUserSharedPreference(User user) {
        SharedPreferences preferences = getApplication().getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(USER_NAME_SHARED_PREF, user.getUser());
        editor.putString(USER_MAIL_SHARED_PREF, user.getMail());
        editor.putInt(USER_ID_SHARED_PREF, user.getId());
        editor.putString(USER_FIRST_NAME_SHARED_PREF, user.getName());
        editor.putString(USER_LAST_NAME_SHARED_PREF, user.getLastName());
        editor.apply();
    }

    public User getUserFromSharedPreferences() {
        User user = new User();
        SharedPreferences preferences =
                getApplication().getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        user.setUser(preferences.getString(USER_NAME_SHARED_PREF, ""));
        user.setId(preferences.getInt(USER_ID_SHARED_PREF, 0));
        user.setMail(preferences.getString(USER_MAIL_SHARED_PREF, ""));
        user.setName(preferences.getString(USER_FIRST_NAME_SHARED_PREF, ""));
        user.setLastName(preferences.getString(USER_LAST_NAME_SHARED_PREF, ""));
        return user;
    }

    public void deleteUserFromSharedPreferences() {
        SharedPreferences preferences =
                getApplication().getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(USER_NAME_SHARED_PREF);
        editor.remove(USER_MAIL_SHARED_PREF);
        editor.remove(USER_ID_SHARED_PREF);
        editor.remove(USER_FIRST_NAME_SHARED_PREF);
        editor.remove(USER_LAST_NAME_SHARED_PREF);
        editor.apply();
    }

    public AlertDialog showMessageWithTitle(String message, String title) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setCancelable(false);
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                (dialog, which) -> dialog.dismiss());
        alertDialog.show();
        return alertDialog;
    }

}

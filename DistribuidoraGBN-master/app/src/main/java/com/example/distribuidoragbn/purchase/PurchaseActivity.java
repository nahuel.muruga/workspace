package com.example.distribuidoragbn.purchase;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.distribuidoragbn.R;
import com.example.distribuidoragbn.base.BaseActivity;
import com.example.distribuidoragbn.base.Constants;
import com.example.distribuidoragbn.productlist.model.Product;
import com.example.distribuidoragbn.purchase.model.ActualPurchase;
import com.example.distribuidoragbn.purchase.model.Purchase;
import com.example.distribuidoragbn.purchase.model.PurchaseDao;

import java.util.ArrayList;

public class PurchaseActivity extends BaseActivity {

    private TextView totalPurchase;
    private TextView purchaseState;
    private Button sendButton;
    private Button cancelButton;
    private PurchaseDao purchaseDao;
    private Purchase onHoldPurchase = null;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase);
        purchaseDao = new PurchaseDao();
        totalPurchase = findViewById(R.id.tv_total_purchase);
        sendButton = findViewById(R.id.btn_send_purchase);
        sendButton.setOnClickListener(event -> {
            if(purchaseDao.savePurchase(ActualPurchase.getInstance().getProductList())) {
                showMessageWithTitle("Pedido realizado", "Éxito").setOnDismissListener(event2 -> finish());
                ActualPurchase.getInstance().cleanPurchase();

            } else {
                showMessageWithTitle("Error al enviar el pedido, intente nuevamente", "Error");
            }
        });
        cancelButton = findViewById(R.id.btn_cancel_purchase);
        cancelButton.setOnClickListener(event -> {
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setCancelable(false);
            alertDialog.setTitle("Descartar compra");
            alertDialog.setMessage("¿Desea eliminar todos los productos de la compra?");
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                    (dialog, which) -> {
                ActualPurchase.getInstance().cleanPurchase();
                finish();
                    });
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancelar",
                    (dialog, which) -> dialog.dismiss());
            alertDialog.show();
        });
        String totalText = "Total = $";
        onHoldPurchase = purchaseDao.getPurchaseWithState(Constants.PurchaseStates.ON_HOLD);
        if(onHoldPurchase != null) {
            sendButton.setVisibility(View.GONE);
            cancelButton.setVisibility(View.GONE);
            setProductList(onHoldPurchase.getProductList());
            totalText = totalText + onHoldPurchase.purchaseTotal();
        } else {
            setProductList(ActualPurchase.getInstance().getProductList());
            totalText = totalText + ActualPurchase.getInstance().actualPurchaseTotal();
        }
        purchaseState = findViewById(R.id.tv_products_title_state);
        purchaseState.setText(getPurchaseState());
        totalPurchase.setText(totalText);
    }

    public void setProductList(ArrayList<Product> productList) {
        recyclerView = findViewById(R.id.rv_product_list);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new ProductListAdapter(productList);
        recyclerView.setAdapter(mAdapter);
    }

    public String getPurchaseState() {
        String purchaseState = "";
        if(ActualPurchase.getInstance().getProductList().size() > 0) {
            purchaseState = Constants.PurchaseStates.IN_PROGRESS;
        }
        if(onHoldPurchase != null) {
            purchaseState = Constants.PurchaseStates.ON_HOLD;
        }
        return "Estado del pedido: " + purchaseState;
    }
}

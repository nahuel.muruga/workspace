package com.example.distribuidoragbn.productlist.model;

import com.example.distribuidoragbn.base.BaseDao;

import java.util.ArrayList;
import java.util.Arrays;

public class ProductDao extends BaseDao {

    public ArrayList<Product> getProductsOfType(String type) {
        //TODO replace for DB script
        ArrayList<Product> products = new ArrayList<>();
        switch (type) {
            case "Leche": {
                Product product = new Product();
                product.setId(1);
                product.setName("Leche la serenísima");
                product.setPrice(100);
                product.setStock(100);
                Product product2 = new Product();
                product2.setId(2);
                product2.setName("Leche la serenísima descremada");
                product2.setPrice(200);
                product2.setStock(200);
                products.add(product);
                products.add(product2);
                break;
            }
            case "Yogures": {
                Product product = new Product();
                product.setId(3);
                product.setName("Yogurt en pote");
                product.setPrice(50);
                product.setStock(100);
                Product product2 = new Product();
                product2.setId(4);
                product2.setName("Yogurt Bebible");
                product2.setPrice(75);
                product2.setStock(15);
                products.add(product);
                products.add(product2);
                break;
            }
        }
        return products;
    }

    public ArrayList<String> getProductTypes() {
        //TODO replace for DB script
        return new ArrayList<>(Arrays.asList("Leche",
                "Quesos",
                "Yogures",
                "Embutidos"));
    }
}

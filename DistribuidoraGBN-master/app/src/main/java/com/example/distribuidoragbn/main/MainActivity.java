package com.example.distribuidoragbn.main;

import android.os.Bundle;

import com.example.distribuidoragbn.R;
import com.example.distribuidoragbn.base.BaseActivity;
import com.example.distribuidoragbn.base.Constants;
import com.example.distribuidoragbn.login.LoginActivity;

import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.MenuItem;

import com.example.distribuidoragbn.login.model.User;
import com.example.distribuidoragbn.productlist.ProductListActivity;
import com.example.distribuidoragbn.purchase.PurchaseActivity;
import com.example.distribuidoragbn.purchase.model.PurchaseDao;
import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    TextView menuTitle;
    TextView menuSubtitle;
    Button btnUserOptions;
    Button btnPurchase;
    Button btnLastPurchase;
    PurchaseDao purchaseDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        btnUserOptions = findViewById(R.id.btn_user_options);
        btnUserOptions.setOnClickListener(event ->
            drawer.openDrawer(GravityCompat.START)
        );
        purchaseDao = new PurchaseDao();
        btnPurchase = findViewById(R.id.btn_purchase);
        btnPurchase.setOnClickListener(event -> {
            goToActivityNoFinish(new ProductListActivity());
        });

        btnLastPurchase = findViewById(R.id.btn_last_purchase);
        btnLastPurchase.setOnClickListener(event -> {
            goToActivityNoFinish(new PurchaseActivity());
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(purchaseDao.getPurchaseWithState(Constants.PurchaseStates.ON_HOLD) != null) {
            disableButton(btnPurchase);
        } else {
            enableButton(btnPurchase);
        }
        if(!purchaseDao.thereIsOnHoldOrInProgressPurchase()) {
            disableButton(btnLastPurchase);
        } else {
            enableButton(btnLastPurchase);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        menuTitle = findViewById(R.id.nav_header_title);
        menuSubtitle = findViewById(R.id.nav_header_subtitle);
        User user = getUserFromSharedPreferences();
        menuTitle.setText(user.getName().concat(" ").concat(user.getLastName()));
        menuSubtitle.setText(user.getMail());
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_logout) {
            deleteUserFromSharedPreferences();
            goToActivity(new LoginActivity());
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void disableButton(Button button) {
        button.setEnabled(false);
    }

    public void enableButton(Button button) {
        button.setEnabled(true);
    }
}

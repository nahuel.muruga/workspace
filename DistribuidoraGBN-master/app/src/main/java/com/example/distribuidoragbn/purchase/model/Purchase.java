package com.example.distribuidoragbn.purchase.model;

import com.example.distribuidoragbn.productlist.model.Product;

import java.util.ArrayList;

public class Purchase {

    private int purchaseId;
    private int userId;
    private ArrayList<Product> productList = new ArrayList<>();
    private String state;

    public int getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(int purchaseId) {
        this.purchaseId = purchaseId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public ArrayList<Product> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<Product> productList) {
        this.productList = productList;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public float purchaseTotal() {
        float total = 0f;
        for (Product product: productList) {
            total = total + (product.getPrice() * product.getQuantity());
        }
        return total;
    }
}

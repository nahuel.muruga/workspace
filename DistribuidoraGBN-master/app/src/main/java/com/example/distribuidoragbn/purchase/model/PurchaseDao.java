package com.example.distribuidoragbn.purchase.model;

import com.example.distribuidoragbn.base.BaseDao;
import com.example.distribuidoragbn.base.Constants;
import com.example.distribuidoragbn.productlist.model.Product;

import java.util.ArrayList;

public class PurchaseDao extends BaseDao {

    public Purchase getPurchaseWithState(String state) {
        //TODO replace for DB script
        return null;
    }

    public boolean savePurchase(ArrayList<Product> purchase) {
        //TODO replace for DB script
        return true;
    }

    public boolean thereIsOnHoldOrInProgressPurchase() {
        return (ActualPurchase.getInstance().getProductList().size() > 0)
                || (getPurchaseWithState(Constants.PurchaseStates.ON_HOLD) != null);
    }
}

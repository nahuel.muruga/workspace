package com.example.distribuidoragbn.login.model;

import com.example.distribuidoragbn.base.BaseDao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDao extends BaseDao {

    public User getUser(String user) {
        Connection conn = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection(DATABASE_URL, DATABASE_USER, DATABASE_PASS);
            PreparedStatement ps = conn
                    .prepareStatement("Select * from USUARIOS where usuario = " + user);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                User u = new User();
                u.setId(rs.getInt("id"));
                u.setUser(rs.getString("usuario"));
                u.setPass(rs.getString("pass"));
                u.setName(rs.getString("nombre"));
                u.setLastName(rs.getString("apellido"));
                u.setMail(rs.getString("email"));
                u.setAdmin(rs.getBoolean("admin"));
                return u;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {

            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}

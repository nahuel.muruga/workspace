package com.example.distribuidoragbn.purchase.model;

import com.example.distribuidoragbn.productlist.model.Product;

import java.util.ArrayList;

public class ActualPurchase {

    private static ActualPurchase instance = null;
    private static ArrayList<Product> productList = new ArrayList<>();

    public static ActualPurchase getInstance() {
        if (instance == null) {
            instance = new ActualPurchase();
        }
        return instance;
    }

    private ActualPurchase() {

    }

    public void removeProduct(Product product) {
        productList.remove(product);
    }

    public void addProduct(Product product) {
        if(existingProduct(product)) {
            for (Product p: productList) {
                if(p.getId() == product.getId()) {
                    p.setQuantity(p.getQuantity() + product.getQuantity());
                }
            }
        } else {
            Product productToAdd = new Product();
            productToAdd.setId(product.getId());
            productToAdd.setName(product.getName());
            productToAdd.setQuantity(product.getQuantity());
            productToAdd.setPrice(product.getPrice());
            productToAdd.setProductType(product.getProductType());
            productToAdd.setStock(product.getStock());
            productList.add(productToAdd);
        }
    }

    public void cleanPurchase() {
        productList.clear();
    }

    public ArrayList<Product> getProductList() {
        return productList;
    }

    public float actualPurchaseTotal() {
        float total = 0f;
        for (Product product: productList) {
            total = total + (product.getPrice() * product.getQuantity());
        }
        return total;
    }

    private boolean existingProduct(Product product) {
        for (Product p: productList) {
            if(p.getId() == product.getId()) {
                return true;
            }
        }
        return false;
    }

    public Product getProductById(int id) {
        for (Product p: productList) {
            if(p.getId() == id) {
                return p;
            }
        }
        return null;
    }
}

package com.example.distribuidoragbn.purchase;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.distribuidoragbn.R;
import com.example.distribuidoragbn.productlist.model.Product;

import java.util.ArrayList;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.MyViewHolder> {

    private ArrayList<Product> mDataSet;

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView productName;
        TextView productQuantity;
        TextView productSubtotal;

        MyViewHolder(View v) {
            super(v);
            productName = v.findViewById(R.id.rv_tv_product_name);
            productQuantity = v.findViewById(R.id.rv_tv_quantity);
            productSubtotal = v.findViewById(R.id.rv_tv_subtotal);
        }
    }

    ProductListAdapter(ArrayList<Product>  myDataset) {
        mDataSet = myDataset;
    }

    @NonNull
    @Override
    public ProductListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_list_item, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.productName.setText(mDataSet.get(position).getName());
        String quantity = "Cantidad: " + mDataSet.get(position).getQuantity();
        holder.productQuantity.setText(quantity);
        String subtotal = "$" + (mDataSet.get(position).getPrice() * mDataSet.get(position).getQuantity());
        holder.productSubtotal.setText(subtotal);
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }
}
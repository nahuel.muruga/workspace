package com.example.distribuidoragbn.login;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.example.distribuidoragbn.base.BaseActivity;
import com.example.distribuidoragbn.login.model.User;
import com.example.distribuidoragbn.login.model.UserDao;
import com.example.distribuidoragbn.main.MainActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.distribuidoragbn.R;

public class LoginActivity extends BaseActivity {

    Button loginButton;
    EditText userName;
    EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        userName = findViewById(R.id.et_user_name);
        password = findViewById(R.id.et_password);
        loginButton = findViewById(R.id.btn_login);
        loginButton.setOnClickListener(e -> {
            if(userName.getText().toString().isEmpty() || password.getText().toString().isEmpty()) {
                loginFail("Complete todos los campos");
            } else {
                login();
            }
        });
        if (getUserFromSharedPreferences().getId() != 0) {
            goToActivity(new MainActivity());
        }
    }

    public void login() {
        UserDao userDao = new UserDao();
        //User user = userDao.getUser(userName.getText().toString());
        //TODO REMOVE THIS STUB
        User user = new User();
        user.setPass("admin");
        user.setId(1);
        user.setName("Nahuel");
        user.setLastName("Muruga");
        user.setMail("Nahuel.Muruga@gmail.com");
        if (user != null && user.getPass().equals(password.getText().toString())) {
            loginSuccess(user);
        } else {
            loginFail("El usuario o la contraseña son incorrectos, por favor ingresarlos nuevamente");
        }
    }

    public void loginSuccess(User user) {
        goToActivity(new MainActivity());
        setUserSharedPreference(user);
    }

    public void loginFail(String errorMessage) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Error");
        alertDialog.setMessage(errorMessage);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                (dialog, which) -> dialog.dismiss());
        alertDialog.show();
    }

}

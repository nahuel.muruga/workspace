package com.example.distribuidoragbn.base;

public class Constants {

    public static class PurchaseStates {
        public static final String CANCELED = "Cancelado";
        public static final String FINISHED = "Finalizado";
        public static final String ON_HOLD = "En Espera";
        public static final String IN_PROGRESS = "En Curso";
    }
}

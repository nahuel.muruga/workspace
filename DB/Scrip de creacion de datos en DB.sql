INSERT INTO `proyectofinal_db`.`estado_civil` (`Estado_CivilDescripcion`) VALUES ('Soltero');
INSERT INTO `proyectofinal_db`.`estado_civil` (`Estado_CivilDescripcion`) VALUES ('Casado');

Insert Into Pedidos_Estados (Descripcion) VALUES ('En Espera');
Insert Into Pedidos_Estados (Descripcion) VALUES ('En Curso');
Insert Into Pedidos_Estados (Descripcion) VALUES ('Finalizado');
Insert Into Pedidos_Estados (Descripcion) VALUES ('Cancelado');

INSERT INTO `proyectofinal_db`.`usuarios` (`Usuario`,`Pass`,`Nombre`,`Apellido`,`Dni`,`Email`,`Telefono`,`Celular`,`Ciudad`,`Domicilio`,`Admin`,`EstCiv`)
VALUES ('Admin','Admin','Test','Test','1231','n@s.com','2341','21312','rosario','uruguay 1077',0,1);

INSERT INTO `proyectofinal_db`.`usuarios` (`Usuario`,`Pass`,`Nombre`,`Apellido`,`Dni`,`Email`,`Telefono`,`Celular`,`Ciudad`,`Domicilio`,`Admin`,`EstCiv`)
VALUES
('Nahuel','Admin','Test','Test','1231','n@s.com','2341','21312','rosario','uruguay 1077',1,1);

INSERT INTO `proyectofinal_db`.`usuarios` (`Usuario`,`Pass`,`Nombre`,`Apellido`,`Dni`,`Email`,`Telefono`,`Celular`,`Ciudad`,`Domicilio`,`Admin`,`EstCiv`)
VALUES
('Empleado','Empleado','Test','Test','1231','n@s.com','2341','21312','rosario','uruguay 1077',0,2);

INSERT INTO `proyectofinal_db`.`productos_tipos` (`Productos_TiposDescricion`) VALUES ('Leche');
INSERT INTO `proyectofinal_db`.`productos_tipos` (`Productos_TiposDescricion`) VALUES ('Quesos');
INSERT INTO `proyectofinal_db`.`productos_tipos` (`Productos_TiposDescricion`) VALUES ('Yogures');
INSERT INTO `proyectofinal_db`.`productos_tipos` (`Productos_TiposDescricion`) VALUES ('Embutidos');
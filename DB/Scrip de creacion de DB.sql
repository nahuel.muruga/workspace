CREATE TABLE IF NOT EXISTS `proyectofinaldb`.`Usuarios` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Usuario` VARCHAR(45) NULL,
  `Pass` VARCHAR(45) NULL,
  `Nombre` VARCHAR(45) NULL,
  `Apellido` VARCHAR(45) NULL,
  `Dni` VARCHAR(45) NULL,
  `EstCiv` VARCHAR(45) NULL,
  `Email` VARCHAR(45) NULL,
  `Telefono` VARCHAR(45) NULL,
  `Celular` VARCHAR(45) NULL,
  `Ciudad` VARCHAR(45) NULL,
  `Domicilio` VARCHAR(45) NULL,
  `Admin` TINYINT NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ProyectoFinal_DB`.`Productos_Tipos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyectofinaldb`.`Productos_Tipos` (
  `idProductos_Tipos` INT NOT NULL AUTO_INCREMENT,
  `Productos_TiposDescricion` VARCHAR(45) NULL,
  PRIMARY KEY (`idProductos_Tipos`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ProyectoFinal_DB`.`Productos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyectofinaldb`.`Productos` (
  `Prod_Id` INT NOT NULL AUTO_INCREMENT,
  `Category` VARCHAR(45) NULL,
  `Prod_Nombre` VARCHAR(45) NULL,
  `Prod_Precio` FLOAT NULL,
  `Prod_Contenido` VARCHAR(45) NULL,
  `Prod_Presentacion` VARCHAR(45) NULL,
  `Prod_Proovedor` VARCHAR(45) NULL,
  `Prod_Telefono` VARCHAR(45) NULL,
  `Prod_Stock` INT NULL,
  `Prod_TipoProd` INT NOT NULL,
  PRIMARY KEY (`Prod_Id`, `Prod_TipoProd`),
  UNIQUE INDEX `Prod_Id_UNIQUE` (`Prod_Id` ASC) ,
  INDEX `fk_Productos_Productos_Tipos1_idx` (`Prod_TipoProd` ASC) ,
  CONSTRAINT `fk_Productos_Productos_Tipos1`
    FOREIGN KEY (`Prod_TipoProd`)
    REFERENCES `ProyectoFinal_DB`.`Productos_Tipos` (`idProductos_Tipos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ProyectoFinal_DB`.`Pedidos_Estados`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyectofinaldb`.`Pedidos_Estados` (
  `PedEstado_Id` INT NOT NULL AUTO_INCREMENT,
  `Descripcion` VARCHAR(45) NULL,
  PRIMARY KEY (`PedEstado_Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ProyectoFinal_DB`.`Pedidos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyectofinaldb`.`Pedidos` (
  `Ped_Id` INT NOT NULL AUTO_INCREMENT,
  `Ped_UsuarioId` INT NOT NULL,
  `Ped_State` INT NOT NULL,
  `Ped_Date` DATETIME NOT NULL,
  PRIMARY KEY (`Ped_Id`, `Ped_State`, `Ped_UsuarioId`),
  INDEX `fk_Pedidos_Pedidos_Estados_idx` (`Ped_State` ASC) ,
  INDEX `fk_Pedidos_Usuarios1_idx` (`Ped_UsuarioId` ASC) ,
  CONSTRAINT `fk_Pedidos_Pedidos_Estados`
    FOREIGN KEY (`Ped_State`)
    REFERENCES `ProyectoFinal_DB`.`Pedidos_Estados` (`PedEstado_Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Pedidos_Usuarios1`
    FOREIGN KEY (`Ped_UsuarioId`)
    REFERENCES `ProyectoFinal_DB`.`Usuarios` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ProyectoFinal_DB`.`Pedidos_Productos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyectofinaldb`.`Pedidos_Productos` (
  `Ped_Id` INT NOT NULL AUTO_INCREMENT,
  `Ped_ProdId` INT NOT NULL COMMENT 'Id del Producto',
  `Ped_ProdCant` INT NULL COMMENT 'Cantidad del Producto',
  `IDPedidos` INT NOT NULL COMMENT 'Id del Pedido',
  PRIMARY KEY (`Ped_Id`, `IDPedidos`, `Ped_ProdId`),
  INDEX `fk_Pedidos_Productos_Pedidos1_idx` (`IDPedidos` ASC) ,
  INDEX `fk_Pedidos_Productos_Productos1_idx` (`Ped_ProdId` ASC) ,
  CONSTRAINT `fk_Pedidos_Productos_Pedidos1`
    FOREIGN KEY (`IDPedidos`)
    REFERENCES `ProyectoFinal_DB`.`Pedidos` (`Ped_Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Pedidos_Productos_Productos1`
    FOREIGN KEY (`Ped_ProdId`)
    REFERENCES `ProyectoFinal_DB`.`Productos` (`Prod_Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE USER 'user1';


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

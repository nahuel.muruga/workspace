package servlets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.*;
import java.io.IOException;
import java.sql.DriverManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class InsertConnTest
 */
public class InsertConnTest  {
	public static void main(String []arg)throws Exception{
	Class.forName("org.apache.derby.jdbc.ClientDriver");
	Connection conn = DriverManager.getConnection("jdbc:derby://localhost/login;create=true");
	String sql="INSERT INTO usuarios (usuario,pass) VALUES  ('nahuel', 'test1') ";
	
	Statement st= conn.createStatement();
	
	int can =st.executeUpdate(sql); /* cuando tengo Insert, Delete, o Update en la sentencia de SQL se utiliza el metodo "Execute.Update"*/
	
	conn.close();
	
	}
}    